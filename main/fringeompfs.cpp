#include "fringe.h"
#include "ioconfig.h"
#include <algorithm>

namespace fringe {
	/**
	 * Naive OpenMP Implementation of Fringe Search with false sharing issues
	 * Locks each vertex when accessing it. Skip neighbours in now list before locking to prevent deadlocks
	 * Locks later list when pushing back an element
	 * @param costRelaxation increment for fThres
	 * @return whether goal was found
	 */
	bool FringeSearchOMPfs(Graph& g, const Vertex& start, const Vertex& goal, const doubleT costRelaxation, const Heuristic& heuristic) {
		doubleT fMin, fThres;
		WeightMap weights = boost::get(boost::edge_weight, g);

		// Used to sort verts by f value
		const auto compareF = [&](Vertex& v1, Vertex& v2) { return g[v1].f < g[v2].f; };

		std::vector<Vertex> now{}, later{};
        const int avgVertDeg = std::ceil(ioconfig::RunConfig::get->avgVertDeg);
		omp_lock_t laterLock;
		omp_init_lock(&laterLock);

		//log/bench variables
		int fringeSizeLog = 0;
		int fringeSizeLogCount = 0;

		now.push_back(start);
		g[start].g = 0;
		g[start].f = g[start].h = heuristic(g[start]);
		g[start].parent = start;

		bool goalFound = false;
		do {
			// computing new threshold,
			fMin = g[*std::min_element(now.begin(), now.end(), compareF)].f;
			fThres = fMin + costRelaxation;

			// --- Prepare Parallel ---
			// determine omp threads used for this iteration, not too many threads
			int activeThreads = std::min(omp_get_max_threads(), std::max(1, (int)now.size() / verticesPerCacheBlock +1));

			//minimise unnecessary reallocations by reserving in advance
            later.resize(later.size() * avgVertDeg, -1);

            // debug variables
//			int expandCount = 0; //Implement with zero padded vector (combine with nextFreeLocalVec in fringeomp.cpp)
//			unsignedT startNowSize = now.size();
//			std::cout << "\nt's: " << activeThreads << ", now{size=" << now.size() << "}: ";
//			for(const auto& it : now)
//				std::cout << it << ", ";
//			std::cout << std::endl;

			// iterate in parallel over the now vector
#pragma omp parallel for num_threads(activeThreads)
			for (unsignedT i = 0; i < now.size(); ++i) {
			    if(goalFound) {// break when goal found (omp hack)
			        i = now.size();
			        continue;
			    }

			    Vertex v = now[i];
//			    std::cout << "lock v:" << v << std::endl;
				omp_set_lock(&g[v].lock);

				if (g[v].f > fThres) {  // skip edges over the f threshold
					omp_set_lock(&laterLock);
					later.push_back(v);
					omp_unset_lock(&laterLock);

//					std::cout << "unlock v:" << v << std::endl;
					omp_unset_lock(&g[v].lock);
					continue;
				}

				if (v == goal) {
					goalFound = true;
//					std::cout << "unlock v:" << v << std::endl;
					omp_unset_lock(&g[v].lock);
					continue;
				}

//				#pragma omp atomic
//				++expandCount;
				g[v].visited = true;

				// Iterate over outgoing edges
				EdgeAdjIt adjEdgeIt, adjEdgeItEnd;
				for (boost::tie(adjEdgeIt, adjEdgeItEnd) = boost::out_edges(v, g); adjEdgeIt != adjEdgeItEnd; ++adjEdgeIt) {
					Vertex neighbour = boost::target(*adjEdgeIt, g);

					if (g[neighbour].visited) //Note! skip vertices in now list, to prevent deadlock
						continue;

					// lock vertex, check that no other thread is accessing this at the same time
//					std::cout << "lock n:" << neighbour << std::endl;
					omp_set_lock(&g[neighbour].lock);

					// compute the travel cost leading up to neighbour(=M in paper)
					doubleT cost = g[v].g + weights[*adjEdgeIt];
					bool improved = cost < g[neighbour].g;

					if (improved) {
                        g[neighbour].h = heuristic(g[neighbour]);
                        g[neighbour].g = cost;
                        g[neighbour].f = cost + g[neighbour].h;
                        g[neighbour].parent = v; // link back to

						omp_set_lock(&laterLock);
						later.push_back(neighbour);
						omp_unset_lock(&laterLock);
                    }

//					std::cout << "unlock n:" << neighbour << std::endl;
					omp_unset_lock(&g[neighbour].lock);
				} // neighbour loop

//				std::cout << "unlock v:" << v << std::endl;
				omp_unset_lock(&g[v].lock);
			} // now loop

//			std::cout << "expandCount: " << expandCount << "/" << startNowSize << "-> later: " << later.size()
//						<< ", fThres: " << fThres << ", activeT's: " << activeThreads << std::endl;
			if(goalFound) {
				*ioconfig::RunConfig::get->csvOut << ", " << std::setw(8) << fringeSizeLog / fringeSizeLogCount;
				return true;
			}

			now.clear(); // does not affect capacity, leaves allocated for next run
            std::swap(now, later);

			fringeSizeLog += now.size();
			++fringeSizeLogCount;
		} while (!now.empty());

		// goal not found
		return false;
	}
} // namespace fringe
