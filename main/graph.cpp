#include "graph.h"

/**
 * Follow parents from goal to start
 * @return shortest path length
 */
void reconstructPathViaParent(Graph& g, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges) {
    path.push_front(goal);
    pathEdges.push_front(edge(goal, g[goal].parent, g).first);
    for (Vertex v = g[goal].parent; ; v = g[v].parent) {
        if (v == start || g[v].parent == v || v == -1 || g[v].parent == -1)
            break;
        Edge e1;
        bool found;
        boost::tie(e1, found) = edge(v, g[v].parent, g);
        path.push_front(g[v].parent);
        pathEdges.push_front(e1);
    }
    path.push_front(start);
}

/**
 * Follow neighbour with lowest fVal from goal to start
 * @return shortest path length
 */
void reconstructPathViaF(Graph& g, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges) {
    path.push_front(goal);
    doubleT gMin = g[goal].g;
    if(gMin == std::numeric_limits<doubleT>::infinity())
    	return;

    Edge e;
    Vertex prev = 0;
    for (Vertex v = goal; v != start; ) {
        EdgeAdjIt adjEdgeIt, adjEdgeItEnd;
        for (boost::tie(adjEdgeIt, adjEdgeItEnd) = boost::out_edges(v, g); adjEdgeIt != adjEdgeItEnd; ++adjEdgeIt) {
            Vertex neighbour = boost::target(*adjEdgeIt, g);
            doubleT gVal = g[neighbour].g;
            if (gVal < gMin) {
                gMin = gVal;
                e = edge(v, neighbour, g).first;
                v = neighbour;
            }
        }
        path.push_front(v);
        pathEdges.push_front(e);
        if(v == prev) {
	        std::cout << "Failed to reconstruct due to loop!";
        	break;
        }
        prev = v;
    }
    path.push_front(start);
}

void reconstructPathAstar(Graph& g, std::vector<Vertex>& p, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges) {
	path.push_front(goal);
	for(Vertex v = goal;; v = p[v]) {
		path.push_front(v);
		pathEdges.push_front(edge(v, p[v], g).first);
		if(p[v] == v)
			break;
	}
	path.push_front(start);
}
