#include "ioconfig.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include <mpi.h>
#include <omp.h>

namespace ioconfig {
    RunConfig* RunConfig::get = nullptr;

    RunConfig::RunConfig() {
        get = this;
    }

	RunConfig ReadConfig(int argc, char **argv, const std::string& defaultFileName) {
		using namespace std;

		RunConfig config{};
		std::string tmp, ignoreLine;
		int iValue = 0;
		double dValue = 0;

		//search argv for -c=* and overwrite the defaultFileName if found
		for (int i = 1; i < argc; ++i) {
			tmp = argv[i];
			if (tmp.rfind("-c=", 0) == 0) {
				config.configFile = tmp.erase(0, 3);
				break;
			}
		}
		if (!config.configFile.empty())
			cout << "alt run.config file: " << tmp << endl;
		config.configFile = config.configFile.empty() ? defaultFileName : config.configFile;

		//Try to open the file
		std::ifstream fin;
		fin.open(config.configFile, ios::in);
		if (fin.bad() || fin.fail()) {
			cerr << "Can't open config file: " + config.configFile << endl;
			cout << "Using default run config." << endl;
			return config;
		}

		/**
		 * Parse params section, search for 'var=value' with regex
		 * Convert match to a stringstream to easily extract the values, ignore whitespace etc
		 * Do this line by line
		 */
		std::regex rgx(R"((\w+)\s*=\s*(.*))");              //Matches  lhs=rhs
		std::smatch match;
		while(getline(fin, tmp)){
			if(regex_search(tmp, match, rgx)){
				const auto& var = match[1];         //0=whole string matched, 1=first capture, 2=second capture
				std::istringstream value(match[2]);

				//Iterate over all possible variables and parse them
				if (var == "OMP_NUM_THREADS") {
					value >> config.numOMPThreads;
					omp_set_num_threads(config.numOMPThreads);
					std::cout << "OMP Threads: " << config.numOMPThreads << std::endl;
				}
				else if (var == "mpi_processes")
					value >> config.numMPIProcs;
				else if(var == "runMode") {
					value >> iValue;
					config.runMode = static_cast<RunMode>(iValue);
				}
				else if(var == "testAlgorithm") {
					value >> iValue;
					config.testAlgorithm = static_cast<SPAlgorithm>(iValue);
				}
				else if (var == "costRelaxation"){
					value >> config.costRelaxation;
				}
				else if (var == "useRandomStartGoal"){
					value >> config.useRandomStartGoal;
				}
				else if(var == "graphType") {
					value >> iValue;
					config.graphType = static_cast<GraphType>(iValue);
				}
				else if (var == "benchIterations")
					value >> config.benchIterations;
				else if (var == "benchFile")
					value >> config.benchFile;
				else if (var == "seed") {
					value >> iValue;
					config.setSeed(iValue);
				}
				else if (var == "gridDimensions") { //read in array
					config.gridDimensions.clear();  //remove default values
					while (value >> iValue) {
						config.gridDimensions.push_back(iValue);
					}
				}
                else if (var == "vertDeg"){
                    value >> config.avgVertDeg;
                }
                else if (var == "numVerts"){
                    value >> config.numVerts;
                }
				else if (var == "randomGraphType"){
					value >> iValue;
					config.randomGraphType = static_cast<RandomGraphType>(iValue);
				}
				else if (var == "gridSize"){
                    value >> config.gridSize;
                }
                else if (var == "normType"){
                    value >> iValue;
                    config.normType = static_cast<NormType>(iValue);
                }
                else if (var == "roadGraph"){
                    value >> iValue;
                    config.roadGraphType = static_cast<RoadGraphType>(iValue);
                }
			}
		}
		if(config.graphType == road){
		    config.normType = Haversine;
		}
		int commSize;
		MPI_Comm_size(MPI_COMM_WORLD, &commSize);
		if(commSize != config.numMPIProcs) {
			config.numMPIProcs = commSize;
			std::cout << "Warning: run.config mpi_processes does not match comm size, using comm size..." << std::endl;
		}

		config.numThreadsProcesses = config.numMPIProcs * config.numOMPThreads;
        if (config.graphType == GraphType::grid) {
            config.avgVertDeg = config.gridDimensions.size();
        }
        if(config.graphType == GraphType::road){
		    config.normType = Haversine;
		}
		return config;
	}

    Norm GetNorm(NormType normType){
        Norm norm;
        if(normType == LInf){
            norm=[](const VertexData& v1, const VertexData& v2) {
                return (v1.pos - v2.pos).maxCoeff();
            };
        }
        else if(normType == L1){
            norm = [](const VertexData& v1, const VertexData& v2){
                return (v1.pos - v2.pos).array().abs().sum();
            };
        }
        else if(normType == L2){
            norm = [](const VertexData& v1, const VertexData& v2){
                return (v1.pos - v2.pos).norm();
            };
        }
        else if(normType == L2Sqr){
	        norm = [](const VertexData& v1, const VertexData& v2){
		        return (v1.pos - v2.pos).squaredNorm();
	        };
        }
        else if(normType == Haversine){
            norm = [](const VertexData& v1, const VertexData& v2){
                doubleT v1Lat = DegreeToRadian(v1.pos[0]);
                doubleT v2Lat = DegreeToRadian(v2.pos[0]);

                doubleT latDiff = DegreeToRadian(v1.pos[0]-v2.pos[0]);
                doubleT lonDiff = DegreeToRadian(v1.pos[1]-v2.pos[1]);
                doubleT s1 = std::sin(latDiff/2);
                doubleT s2 = std::sin(lonDiff/2);
                doubleT a = s1 * s1 + std::cos(v1Lat) * std::cos(v2Lat) * s2 * s2;
                doubleT c = 2 * std::atan2(std::sqrt(a), std::sqrt(1-a));
                return c * 6372.8;
            };
        }
        return norm;
    }

    Heuristic GetHeuristic(NormType normType, Position& goalPos){
		Heuristic h;
	    if(normType == LInf){
		    h=[&](const VertexData& v1) {
			    return (v1.pos - goalPos).maxCoeff();
		    };
	    }
	    else if(normType == L1){
		    h = [&](const VertexData& v1){
			    return (v1.pos - goalPos).array().abs().sum();
		    };
	    }
	    else if(normType == L2){
		    h = [&](const VertexData& v1){
			    return (v1.pos - goalPos).norm();
		    };
	    }
	    else if(normType == L2Sqr){
		    h = [&](const VertexData& v1){
			    return (v1.pos - goalPos).squaredNorm();
		    };
	    }
	    else if(normType == Haversine){
	        h = [&](const VertexData& v){
                doubleT vLat = DegreeToRadian(v.pos[0]);
                doubleT goalLat = DegreeToRadian(goalPos[0]);

                doubleT latDiff = DegreeToRadian(v.pos[0]-goalPos[0]);
                doubleT lonDiff = DegreeToRadian(v.pos[1]-goalPos[1]);
                doubleT s1 = std::sin(latDiff/2);
                doubleT s2 = std::sin(lonDiff/2);
                doubleT a = s1 * s1 + std::cos(vLat) * std::cos(goalLat) * s2 * s2;
                doubleT c = 2 * std::atan2(std::sqrt(a), std::sqrt(1-a));
                return c * 6372.8;
	        };
	    }
	    return h;
	}

} // namespace ioconfig

