#include "astar.h"

namespace astar{

	using namespace boost;

    bool AStarTest(const Graph& g, Vertex& start, Vertex& goal, Norm norm){
        std::vector<Vertex> pred(num_vertices(g));
        std::vector<doubleT> dist(num_vertices(g));

        bool goalFound = false;
        try{
            boost::astar_search(g, start, AStarNorm(norm, goal, g),
                                weight_map(get(edge_weight, g)).
                                predecessor_map(pred.data()).distance_map(dist.data()).
                                visitor(AstarGoalVisitor(goal)));
        }catch(foundGoal) { // found a path to the goal
            goalFound = true;
        }

	    std::cout << std::endl << "Total travel time: " << dist[goal] << std::endl;
	    return goalFound;
    }

    bool AStarBench(const Graph& g, Vertex& start, Vertex& goal, Norm norm, std::vector<Vertex>& pred, std::vector<doubleT>& dist){
	    bool goalFound = false;

        try{
	        boost::astar_search(g, start, AStarNorm(norm, goal, g),
	                            weight_map(get(edge_weight, g)).
	                            predecessor_map(pred.data()).distance_map(dist.data()).
			                    visitor(AstarGoalVisitor(goal)));
        }catch(foundGoal) { // found a path to the goal
            goalFound = true;
        }
	    return goalFound;
    }
} // namespace astar