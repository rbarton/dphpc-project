#include "fringe.h"
#include <algorithm>
#include "ioconfig.h"
#include <chrono>

namespace fringe {
	/**
	 * Sequential Implementation of Fringe Search
	 * @param costRelaxation increment for fThres
	 * @return whether goal was found
	 */
	bool FringeSearchSeq(Graph& g, const Vertex& start, const Vertex& goal, const doubleT costRelaxation, const Heuristic& heuristic) {
		doubleT fMin, fThres;
		WeightMap weights = boost::get(boost::edge_weight, g);

		// Use std::list, has O(1) insert, erase, usually implemented as a doubly linked list
		alignas(4096) std::vector<Vertex> now{}, later{};

		//log/bench variables
		int fringeSizeLog = 0;
		int fringeSizeLogCount = 0;

		// Initialise with start
		now.push_back(start);
		g[start].g = 0;
		g[start].f = g[start].h = fMin = heuristic(g[start]);
		g[start].parent = start;

		do {
//			using namespace std::chrono;
//			auto startTime = high_resolution_clock::now();
			// computing new threshold
			//fMin = g[*std::min_element(now.begin(), now.end(), compareF)].f;
			fThres = fMin + costRelaxation;
            fMin = std::numeric_limits<doubleT>::infinity();
			// debug variables
//			unsignedT expandCount = 0;
//			unsignedT startNowSize = now.size();

			const int avgVertDeg = static_cast<const int>(std::ceil(ioconfig::RunConfig::get->avgVertDeg));
			later.reserve(now.size() * avgVertDeg);

//			auto t1 = high_resolution_clock::now();
			// iterate over *current* now list once
			for (unsignedT i = 0; i < now.size(); ++i) {
				Vertex& v = now[i];

				if (g[v].f > fThres) {  // skip edges over the f threshold
					fMin = std::min(g[v].f, fMin);
					later.push_back(v);
					continue;
				}

				if (v == goal) {
					*ioconfig::RunConfig::get->csvOut << ", " << std::setw(8) << fringeSizeLog / fringeSizeLogCount;
					return true;
				}
//				++expandCount;

				// marking node as closed
				g[v].visited = true;

				// Iterate over outgoing edges (better than iterating over neighbours as we can get vertices easily)
				EdgeAdjIt adjEdgeIt, adjEdgeItEnd;
				for (boost::tie(adjEdgeIt, adjEdgeItEnd) = boost::out_edges(v, g); adjEdgeIt != adjEdgeItEnd; ++adjEdgeIt) {
					Vertex neighbour = boost::target(*adjEdgeIt, g);

					// ignore closed nodes
					if (g[neighbour].visited)
						continue;

					// compute the travel cost leading up to neighbour(=M in paper)
					doubleT cost = g[v].g + weights[*adjEdgeIt];
					bool improved = cost < g[neighbour].g;

					if (improved) {
						g[neighbour].h = heuristic(g[neighbour]);
						g[neighbour].g = cost;
						g[neighbour].f = cost + g[neighbour].h;
						fMin = std::min(g[neighbour].f, fMin);
						g[neighbour].parent = v;

						later.push_back(neighbour);
					}
				} // neighbour loop
			} // now loop
//			auto t2 = high_resolution_clock::now();
			now.clear();
//			std::cout << "expandCount: " << expandCount << "/" << startNowSize << "-> later: " << later.size()
//						<< ", fThres: " << fThres << std::endl;
			std::swap(now, later);
//			auto t3 = high_resolution_clock::now();
//			std::cout << duration_cast<duration<double>>(t3 - startTime).count() << ", "
//			          << duration_cast<duration<double>>(t1 - startTime).count() << ", "
//			          << duration_cast<duration<double>>(t2 - t1).count() << ", "
//			          << duration_cast<duration<double>>(t3 - t2).count() << ", s" << std::endl;

			fringeSizeLog += now.size();
			++fringeSizeLogCount;
		} while (!now.empty());

		// goal not found
		return false;
	}
} // namespace fringe
