#include "fringe.h"
#include "ioconfig.h"
#include <algorithm>
#include <mpi.h>
#include <numeric>
#include <chrono>

namespace fringe {

	 /**
	  * Data to be communicated between MPI ranks
	  */
	struct ModifiedVertex {
		Vertex vertex;
		doubleT g;
	};
	const auto modifiedVerticesPerCacheBlock = elementsPerCacheBlock<ModifiedVertex>();
	const unsignedT localBlockSize = 4; //in #cache blocks

	/**
	 * Private data for each omp thread
	 * Aligned to a cacheblock to prevent false-sharing
	 */
	struct alignas((int)pow(2, ceil(log(3 * sizeof(unsignedT) + 3 * 64 * localBlockSize)/log(2)))) ThreadData {
		unsignedT nextFreeIndex = 0;
		unsignedT nextFreeIndexModified = 0;
		unsignedT nextFreeIndexVisited = 0;

		Vertex laterBlock[verticesPerCacheBlock * localBlockSize];
		ModifiedVertex modifiedBlock[modifiedVerticesPerCacheBlock * localBlockSize];
		Vertex visitedBlock[verticesPerCacheBlock * localBlockSize];
	};

	 /**
	  * Writes (partly filled) local block to shared vector
	  * Note! once a partly filled block is added you loose the alignment -> false sharing, only add these at the end
	  */
	 template <class elemType>
	 void OMP_updateVector(std::vector<elemType>& vector, elemType*& block, omp_lock_t& vectorLock, unsignedT& nextFreeLocal) {
	 	if (nextFreeLocal == 0)
	 		return;
         // Resize vector and then copy outside of critical section
		 omp_set_lock(&vectorLock);
		 const auto size = vector.size();
         vector.resize(size + nextFreeLocal);
		 omp_unset_lock(&vectorLock);

		 std::copy(block, block + nextFreeLocal, vector.begin() + size);
		 nextFreeLocal = 0;
	 }

    /**
     * False-sharing-free push_back for vector of type Vertex
     * If block is full write to shared vector and start "fresh" block
     */
	template <typename T>
    void OMP_PushBack(std::vector<T>& vector, T*& block, omp_lock_t& vectorLock, const T& v, unsignedT& nextFreeLocal) {
        block[nextFreeLocal++] = v;
        if (nextFreeLocal % (elementsPerCacheBlock<T>() * localBlockSize) == 0)
            OMP_updateVector(vector, block, vectorLock, nextFreeLocal);
    }

    /**
     * 1. Communicate sizes of each ranks' modified first via allgather
     * 2. Compute prefix sum of sizes, so we can use this as offsets for where to add each array into the modifiedGlobal
     *    Do this exclusively so we get [0, ..., sum of all sizes], with length ranks + 1
     * 3. Allgatherv to communicate each modified vector into modifiedGlobal (v for varying sizes)
     * 4. Apply modifications to the graph, skip our own rank in the vector
     *
     * Note: need to be careful if a rank has no modifications
     */
     void sendModifiedVisited(const int rank, MPI_Request& modifiedRequest, MPI_Request& visitedRequest, MPI_Datatype MPI_Modified_Vertex,
                              std::vector<unsignedT>& modifiedSizes, std::vector<ModifiedVertex>& modified, std::vector<ModifiedVertex>& modifiedGlobal,
                              std::vector<unsignedT>& visitedSizes, std::vector<Vertex>& visited, std::vector<Vertex>& visitedGlobal,
                              std::vector<unsignedT>& modifiedSizesPrefix, std::vector<unsignedT>& visitedSizesPrefix) {
        MPI_Request modifiedSizesRequest, visitedSizesRequest;
     	modifiedSizes[rank] = modified.size();
        MPI_Iallgather(MPI_IN_PLACE, 1, MPI_UNSIGNED, modifiedSizes.data(), 1, MPI_UNSIGNED, MPI_COMM_WORLD, &modifiedSizesRequest);
	    visitedSizes[rank] = visited.size();
	    MPI_Iallgather(MPI_IN_PLACE, 1, MPI_UNSIGNED, visitedSizes.data(), 1, MPI_UNSIGNED, MPI_COMM_WORLD, &visitedSizesRequest);

	    MPI_Wait(&modifiedSizesRequest, MPI_STATUS_IGNORE);
        modifiedSizesPrefix.resize(modifiedSizes.size() + 1);
        modifiedSizesPrefix[0] = 0;
        std::partial_sum(modifiedSizes.begin(), modifiedSizes.end(),
                         modifiedSizesPrefix.begin() + 1);  // do exclusive prefix sum
        modifiedGlobal.resize(modifiedSizesPrefix[modifiedSizesPrefix.size() - 1]);

        MPI_Iallgatherv(modified.data(), modified.size(), MPI_Modified_Vertex, modifiedGlobal.data(),
                        reinterpret_cast<const int *>(modifiedSizes.data()),  // count of recv data
                        reinterpret_cast<const int *>(modifiedSizesPrefix.data()),  // displacement
                        MPI_Modified_Vertex, MPI_COMM_WORLD, &modifiedRequest);

        /**
         * Same for Visited
         */
	    MPI_Wait(&visitedSizesRequest, MPI_STATUS_IGNORE);
        visitedSizesPrefix.resize(visitedSizes.size() + 1);
        visitedSizesPrefix[0] = 0;
        std::partial_sum(visitedSizes.begin(), visitedSizes.end(),
                         visitedSizesPrefix.begin() + 1);  // do exclusive prefix sum

        visitedGlobal.resize(visitedSizesPrefix[visitedSizesPrefix.size() - 1]);

        MPI_Iallgatherv(visited.data(), visited.size(), MPI_UNSIGNED, visitedGlobal.data(),
                        reinterpret_cast<const int *>(visitedSizes.data()),  // count of recv data
                        reinterpret_cast<const int *>(visitedSizesPrefix.data()),  // displacement
                        MPI_UNSIGNED, MPI_COMM_WORLD, &visitedRequest);
    }

	/**
	 * Hybrid MPI+OpenMP Implementation of Fringe Search
	 * @param costRelaxation increment for fThres
	 * @return whether goal was found
	 */
	bool FringeSearchMPI(Graph& g, const Vertex& start, const Vertex& goal, const doubleT costRelaxation, const Heuristic& heuristic) {
		int rank, commSize;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &commSize);

		// create custom MPI Datatype to send/recv vertex ID and g value of modified vertex
        const int blockLengths[] = {1, 1};
		MPI_Datatype tmpType, MPI_Modified_Vertex;
		MPI_Aint offsets[] = {offsetof(ModifiedVertex, vertex), offsetof(ModifiedVertex, g)};
		MPI_Datatype types[] = {MPI_UNSIGNED, MPI_FLOAT};
        MPI_Aint lb, extent;
		MPI_Type_create_struct(2, blockLengths, offsets, types, &tmpType);
        MPI_Type_get_extent(tmpType, &lb, &extent);
        MPI_Type_create_resized(tmpType, lb, extent, &MPI_Modified_Vertex);
        MPI_Type_commit(&MPI_Modified_Vertex);

		alignas(4096) std::vector<Vertex> now{}, later{};
		omp_lock_t laterLock;
		omp_init_lock(&laterLock);
		alignas(4096) std::vector<ModifiedVertex> modified{};
        omp_lock_t modifiedLock;
        omp_init_lock(&modifiedLock);
		alignas(4096) std::vector<Vertex> visited{};
        omp_lock_t visitedLock;
        omp_init_lock(&visitedLock);

		std::vector<unsignedT> nowSizes(commSize, 0);
		unsignedT nowSizeGlobal = 0;
		MPI_Request nowSizeRequest;
		std::vector<unsignedT> modifiedSizes(commSize, 0);
        std::vector<unsignedT> modifiedSizesPrefix{};
        std::vector<unsignedT> visitedSizes(commSize, 0);
        std::vector<unsignedT> visitedSizesPrefix{};

        std::vector<MPI_Request> loadBalanceRequests;
		std::vector<ModifiedVertex> modifiedGlobal;
		MPI_Request modifiedRequest;
		std::vector<Vertex> visitedGlobal;
		MPI_Request visitedRequest;

		WeightMap weights = boost::get(boost::edge_weight, g);
		const int avgVertDeg = static_cast<const int>(std::ceil(ioconfig::RunConfig::get->avgVertDeg));
		bool goalFound = false;
		MPI_Request goalFoundRequest;
		doubleT fMin = std::numeric_limits<doubleT>::infinity();
		MPI_Request fMinRequest;
		doubleT fThres;

		//log/bench variables
		int fringeSizeLog = 0;
		int fringeSizeLogCount = 0;
		std::ofstream ratioCsv;
		if(ioconfig::RunConfig::get->runMode == ioconfig::RunMode::debug) {
			ratioCsv.open(std::to_string(rank).append("mpi2omp.csv"));
			ratioCsv << "MpiTOverOmpT,OMPTime,iterTime,nowSize" << std::endl;
		}

		// start with just start vertex in rank 0
		if (rank == 0) {
            now.push_back(start);
            g[start].g = 0;
            g[start].f = g[start].h = fMin = heuristic(g[start]);
            g[start].parent = start;
        }

//  --------------------------------------------------------------------------------------------------------------------
//  ---------------------------------------- Start of do-while ---------------------------------------------------------

		int iter = 0;
		do {
			auto startTime = std::chrono::high_resolution_clock::now();

			//TODO: doesn't work, modifiedGlobal is empty in WaitOnModifiedVisited
//			sendModifiedVisited(rank, modifiedRequest, visitedRequest, MPI_Modified_Vertex, modifiedSizes, modified,
//	            modifiedGlobal, visitedSizes, visited, visitedGlobal, modifiedSizesPrefix, visitedSizesPrefix);

            fThres = fMin + costRelaxation;
			fMin = std::numeric_limits<doubleT>::infinity();

            // ---------- Prepare Parallel OMP ----------------------
            // determine omp threads used for this iteration, not too many threads
            int activeThreads = std::min(omp_get_max_threads(),
                                         std::max(1, (int) now.size() / verticesPerCacheBlock + 1));
            //TODO: how to choose this? (is actually indep. of cacheBlock size)

            // stores where each thread is inserting in the later, modified and visited block, zero padded for false sharing via 2D vector
            std::vector<ThreadData> threadData(activeThreads, ThreadData{});

            later.reserve(std::max<unsignedT>(now.size() * avgVertDeg, (activeThreads + 1) * verticesPerCacheBlock));
            modified.reserve(now.size() * std::sqrt(avgVertDeg)); //just an estimate
            visited.reserve(now.size());

//  --------------------------------------------------------------------------------------------------------------------
//  ---------------------------------------- Start of parallel for -----------------------------------------------------

            #pragma omp parallel for num_threads(activeThreads) reduction(min : fMin)
			for (unsignedT i = 0; i < now.size(); ++i) {
				if (goalFound) {  // break when goal found (omp hack)
					i = now.size();
					continue;
				}

	            Vertex &v = now[i];
	            bool skipVisited;
				#pragma atomic read
	            skipVisited = g[v].visited;
	            if (skipVisited) {
//		            std::cout << iter << " r" << rank << " skipVis:" << v << std::endl;
		            continue;
	            }

	            unsignedT tid = omp_get_thread_num();
                unsignedT& nextFreeLocal = threadData[tid].nextFreeIndex;
                unsignedT& nextFreeLocalModified = threadData[tid].nextFreeIndexModified;
                unsignedT& nextFreeLocalVisited = threadData[tid].nextFreeIndexVisited;
                Vertex* laterBlock = threadData[tid].laterBlock;
                ModifiedVertex* modifiedBlock = threadData[tid].modifiedBlock;
                Vertex* visitedBlock = threadData[tid].visitedBlock;

                doubleT fVal;
                #pragma omp atomic read
                fVal = g[v].f;

                if (fVal > fThres) {  // skip edges over the f threshold
                    fMin = std::min(fVal, fMin);
                    OMP_PushBack(later, laterBlock, laterLock, v, nextFreeLocal);
                    continue;
                }

                if (v == goal) {
                    goalFound = true;
                	std::cout << "r" << rank << "t" << omp_get_thread_num() << " found goal" << std::endl;
                    continue;
                }

                #pragma omp atomic write
                g[v].visited = true;
                OMP_PushBack(visited, visitedBlock, visitedLock, v, nextFreeLocalVisited);
                // TODO: should we exchange visited before starting neighbour loop, since we skip already visited neighbours?

                doubleT gVal;
                #pragma omp atomic read
                gVal = g[v].g;

                // ---------- Start of neighbour loop ---------------
                // Iterate over outgoing edges (better than iterating over neighbours as we can get vertices easily)
                EdgeAdjIt adjEdgeIt, adjEdgeItEnd;
                for (boost::tie(adjEdgeIt, adjEdgeItEnd) = boost::out_edges(v, g); adjEdgeIt != adjEdgeItEnd; ++adjEdgeIt) {
                    Vertex neighbour = boost::target(*adjEdgeIt, g);
                    bool skip;
                    #pragma atomic read
                    skip = g[neighbour].visited;
                    if (skip)
                        continue;

                    // lock vertex, check that no other thread is accessing this at the same time
                    omp_set_lock(&g[neighbour].lock);

                    // compute the travel cost leading up to neighbour
                    doubleT cost = gVal + weights[*adjEdgeIt];
	                bool improved = cost < g[neighbour].g;

                    if (improved) {
                        g[neighbour].h = heuristic(g[neighbour]);
                        g[neighbour].g = cost;
                        g[neighbour].f = cost + g[neighbour].h;
                        fMin = std::min(g[neighbour].f, fMin);
//                        g[neighbour].parent = v; // link back to
                        //TODO: Can modify a vertex several times on the same process, get several elements in modified
                        OMP_PushBack(modified, modifiedBlock, modifiedLock, ModifiedVertex{neighbour, g[neighbour].g}, nextFreeLocalModified);
                        OMP_PushBack(later, laterBlock, laterLock, neighbour, nextFreeLocal);
                    }
                    omp_unset_lock(&g[neighbour].lock);
                }
                // ---------- End of neighbour loop -----------------

            }
//  ---------------------------------------- End of parallel for -------------------------------------------------------
//  --------------------------------------------------------------------------------------------------------------------
			MPI_Iallreduce(MPI_IN_PLACE, &goalFound, 1, MPI_CXX_BOOL, MPI_LOR, MPI_COMM_WORLD, &goalFoundRequest);

            // write remaining nodes to shared vectors, for each thread
            for (int t = 0; t < threadData.size(); ++t) {
                unsignedT& nextFreeLocal = threadData[t].nextFreeIndex;
                unsignedT& nextFreeLocalModified = threadData[t].nextFreeIndexModified;
                unsignedT& nextFreeLocalVisited = threadData[t].nextFreeIndexVisited;

                Vertex* laterBlock = threadData[t].laterBlock;
                ModifiedVertex* modifiedBlock = threadData[t].modifiedBlock;
                Vertex* visitedBlock = threadData[t].visitedBlock;

                //TODO: Dont use locks here
                OMP_updateVector(later, laterBlock, laterLock, nextFreeLocal);
                OMP_updateVector(modified, modifiedBlock, modifiedLock, nextFreeLocalModified);
                OMP_updateVector(visited, visitedBlock, visitedLock, nextFreeLocalVisited);
            }
			auto tomp = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double> ompTime = tomp - startTime;

//  --------------------------------------------------------------------------------------------------------------------
//  ---------------------------------------- Start of MPI communication ------------------------------------------------

          sendModifiedVisited(rank, modifiedRequest, visitedRequest, MPI_Modified_Vertex, modifiedSizes, modified,
		            modifiedGlobal, visitedSizes, visited, visitedGlobal, modifiedSizesPrefix, visitedSizesPrefix);

            /**
             * Function to wait/receive data
             * Need to use a function as we should call this if we have found the goal or at end of do loop
             */
			const auto& WaitOnModifiedVisited = [&]() {
				/**
				 * A. Update Modified Vertices
				 */
				MPI_Wait(&modifiedRequest, MPI_STATUS_IGNORE);
//				std::cout << iter << " r" << rank << " modifiedGlobal: ";
//				for (const auto i : modifiedGlobal)
//					std::cout << "(" << i.vertex << "," << i.g << "), ";
//				std::cout << std::endl;

				//Apply modifications to the graph, skip own rank modifications
				for (int i = 0; i < modifiedSizesPrefix[rank]; ++i) {
					g[modifiedGlobal[i].vertex].g = std::min(g[modifiedGlobal[i].vertex].g, modifiedGlobal[i].g);
					g[modifiedGlobal[i].vertex].h = heuristic(g[modifiedGlobal[i].vertex]);
					g[modifiedGlobal[i].vertex].f = g[modifiedGlobal[i].vertex].g + g[modifiedGlobal[i].vertex].h;
				}

				for (int i = modifiedSizesPrefix[rank + 1]; i < modifiedGlobal.size(); ++i) {
					g[modifiedGlobal[i].vertex].g = std::min(g[modifiedGlobal[i].vertex].g, modifiedGlobal[i].g);
					g[modifiedGlobal[i].vertex].h = heuristic(g[modifiedGlobal[i].vertex]);
					g[modifiedGlobal[i].vertex].f = g[modifiedGlobal[i].vertex].g + g[modifiedGlobal[i].vertex].h;

				}

				modified.clear();
				modifiedGlobal.clear();

				/**
				 * B. Update Visited
				 */
				MPI_Wait(&visitedRequest, MPI_STATUS_IGNORE);
				//Apply modifications to the graph, skip own rank modifications
//				std::cout << iter << " r" << rank << " visitedGlob : ";
//				for (const auto i : visitedGlobal)
//					std::cout << i << ", ";
//				std::cout << std::endl;

				for (int i = 0; i < visitedSizesPrefix[rank]; ++i)
					g[visitedGlobal[i]].visited = true;

				for (int i = visitedSizesPrefix[rank + 1]; i < visitedGlobal.size(); ++i)
					g[visitedGlobal[i]].visited = true;


				visited.clear();
				visitedGlobal.clear();
			};

			MPI_Wait(&goalFoundRequest, MPI_STATUS_IGNORE);
			if (goalFound){
//				//Need to wait and send again if we send before omp for
//				WaitOnModifiedVisited();
//				sendModifiedVisited(rank, modifiedRequest, visitedRequest, MPI_Modified_Vertex, modifiedSizes, modified,
//				                    modifiedGlobal, visitedSizes, visited, visitedGlobal, modifiedSizesPrefix, visitedSizesPrefix);

				WaitOnModifiedVisited();
				if(rank == 0)
					*ioconfig::RunConfig::get->csvOut << ", " << std::setw(8) << fringeSizeLog / fringeSizeLogCount;
				return true;
			}

            // ---------- Communicate later sizes -------------------
			/**
			 * Get all later sizes so we can rebalance below
			 */
			nowSizes[rank] = later.size();
			MPI_Iallgather(MPI_IN_PLACE, 1, MPI_UNSIGNED, nowSizes.data(), 1, MPI_UNSIGNED, MPI_COMM_WORLD, &nowSizeRequest);
            // ------------------------------------------------------

            // ---------- Reduce fMin over all threads/procs --------
			/**
			 * Get fMin over all ranks via allreduce
			 */
			MPI_Iallreduce(MPI_IN_PLACE, &fMin, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD, &fMinRequest);
            // ------------------------------------------------------

            now.clear();  // does not affect capacity, leaves allocated for next run
            std::swap(now, later);

            // ---------- Balance rank loads ------------------------
            /**
             * Balance by matching ranks by lowest and highest load
             * 1. Sort nowSizes but keep the id of the rank, so we use a pair (NowSizePair)
             * 2. Do sum of nowSizes to get mean load
             * 3. Move so each rank has ~mean
             *    1. Donate work to left most until its at the mean
             *    2. Take from the right most until its also at the mean
             */
			MPI_Wait(&nowSizeRequest, MPI_STATUS_IGNORE);
			nowSizeGlobal = std::accumulate(nowSizes.begin(), nowSizes.end(), 0u);
			unsignedT mean = nowSizeGlobal / nowSizes.size();

			using NowSizePair = std::pair<unsignedT, unsignedT>;    //(size, rank)
            std::vector<NowSizePair> nowSizesSorted(nowSizes.size());
			for (unsignedT j = 0; j < nowSizes.size(); ++j)
				nowSizesSorted[j] = NowSizePair(nowSizes[j], j);

            std::sort(nowSizesSorted.begin(), nowSizesSorted.end()); //sorts by first

			/**
			 * Function to send or receive now, checks if this rank participates and resizes accordingly
			 */
			const auto SendRecvNow = [&](const unsignedT& left, const unsignedT& right, const unsignedT& sendCount, const int& tag){
				if(nowSizesSorted[left].second == rank){
//					std::cout << iter << " r" << rank << " recv: " << sendCount << std::endl;
					int nowSize = now.size();
					now.resize((unsignedT)now.size() + sendCount); //Note: this should not reallocate the array as we have reserved (invalidating previous recv ptrs)
					loadBalanceRequests.emplace_back(MPI_Request{});
					MPI_Irecv(now.data() + nowSize, sendCount, MPI_UNSIGNED, nowSizesSorted[right].second, tag, MPI_COMM_WORLD, &loadBalanceRequests[loadBalanceRequests.size() - 1]);
				}
				else if (nowSizesSorted[right].second == rank){
//					std::cout << iter << " r" << rank << " send: " << sendCount << std::endl;
					loadBalanceRequests.emplace_back(MPI_Request{});
					MPI_Isend(now.data() + now.size() - sendCount, sendCount, MPI_UNSIGNED, nowSizesSorted[left].second, tag, MPI_COMM_WORLD, &loadBalanceRequests[loadBalanceRequests.size() - 1]);
					now.resize(now.size() - sendCount);
				}
			};

			/**
			 * Ensure there is space available
			 * So we don't reallocate during a recv and invalidate the pointers (!)
			 */
			now.reserve(mean);

			unsignedT left = 0, right = nowSizesSorted.size() -1;
			unsignedT rebalanceThreshold = verticesPerCacheBlock * 16; //TODO: choose this theshold?
			int tag = 10; //increment the tag so we can match multiple send/recv uniquely
			while(left < right && nowSizesSorted[right].first - nowSizesSorted[left].first > rebalanceThreshold) {
				if (mean - nowSizesSorted[left].first > nowSizesSorted[right].first) {
					unsignedT sendCount = nowSizesSorted[right].first - mean;
					SendRecvNow(left, right, sendCount, tag);
					nowSizesSorted[right].first -= sendCount;
					++left;
				} else {
					unsignedT sendCount = mean - nowSizesSorted[left].first;
					SendRecvNow(left, right, sendCount, tag);
					nowSizesSorted[left].first += sendCount;
					--right;
				}
				++tag;
			}
            // ------------------------------------------------------

            // Wait on and process received data
            WaitOnModifiedVisited();

			// Wait for now to be received/sent
			MPI_Waitall(loadBalanceRequests.size(), loadBalanceRequests.data(), MPI_STATUS_IGNORE);
			loadBalanceRequests.clear();
			MPI_Wait(&fMinRequest, MPI_STATUS_IGNORE);

//			std::cout << iter << " r" << rank << " now: " << now.size() << ", mean: " << mean << ", fmin: " << fMinGlobal << std::endl;

//			std::cout << iter << " r" << rank << " now[" << now.size() << "] : ";
//			for (const auto i : now)
//				std::cout << i << ", ";
//			std::cout << std::endl;
//
//			std::cout << std::endl;

			//bench logging
			auto endTime = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double> mpiTime = endTime - tomp;

			if(ioconfig::RunConfig::get->runMode == ioconfig::RunMode::debug) {
				ratioCsv << std::setw(12) << (mpiTime.count() / ompTime.count()) << ", "
				         << std::setw(12) << ompTime.count() << ", "
				         << std::setw(12) << (mpiTime.count() + ompTime.count()) << ", "
				         << std::setw(8) << now.size() << std::endl;
			}

			fringeSizeLog += nowSizeGlobal;
			++fringeSizeLogCount;

			++iter;
		} while (nowSizeGlobal > 0);
//  ---------------------------------------- End of do-while -----------------------------------------------------------
//  --------------------------------------------------------------------------------------------------------------------


		return false; // goal not found
	}
} // namespace fringe
