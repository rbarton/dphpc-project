#pragma once
#include "graph.h"
#include "ioconfig.h"
#include <Eigen/Dense>
#include <random>
#include <iostream>

// -------- Printing to GraphViz ---------------
/**
 * Functions related to importing/generating/printing graphs (uses graphviz file format)
 */
namespace iograph {
	/**
	 * Generates and n-D grid graph. All edges have weight 1. No diagonal edges
	 * @param rng random number generator
	 */
	void GenerateGridGraph(Graph& g, ioconfig::RunConfig runConfig, std::mt19937& rng);

	/**
	 * Generates fully connected graph with randomly located vertices
	 * @param numberVertices
	 * @param vertDeg double that gives the mean vertex degree
	 * @param girdSize size of rectangle
	 * @param randomGraphType 1 for uniform, 2 for graph with two clusters, 3 for snowman
	 * @param rng random number generator
	 */
	void GenerateRandomGraph(Graph& g, ioconfig::RunConfig& runConfig, std::mt19937 & rng);

    /**
     * Generates a graph of a road map based on open street maps
     * source: https://www.cise.ufl.edu/research/sparse/matrices/DIMACS10/
     * @param g graph
     * @param runConfig
     */
    void GenerateRoadGraph(Graph& g, ioconfig::RunConfig& runConfig);




    //------------ Helper Functions ------------------------------------------------------------------------------
    /**
     * Constructs a property map for all the weights
     * @param g
     * @return WeightMap
     */
    const WeightVec GetWeightVec(const  Graph& g);

    /**
     * Prints min, max, average weight
     * @param g
     */
    void WeightInfo(const Graph& g);

    /**
     * count all degrees
     * @param g
     * @return map with key=degree and value=count
     */
    const DegreeMap GetDegrees(const Graph& g);

    void DegreeInfo(const Graph& g);
		// ------- Graph Printing ------------------------------------------------------------------------------------------

	/**
	 * Writes GraphViz properties for whole graph, e.g. line style
	 */
	struct GraphWriter {
		void operator()(std::ostream& out) const {
//			out << "graph [bgcolor=lightgrey]" << std::endl;
			out << "node [penwidth=4]" << std::endl;
//			out << "edge [style=dashed]" << std::endl;
		}
	};

	/**
	 * This is used to define what/how to print a vertex to the graphviz format
	 * Sets color, pos of a vertex in the output in the operator() evaluated for each vertex
	 * @tparam VertexDataMap container for VertexData
	 */
	template<class VertexDataMap>
	class VertexWriter {
	public:
		VertexWriter(Vertex& start_, Vertex& goal_, Graph& g_, std::list<Vertex>& coloredVerts_, double scale_)
				: start(start_), goal(goal_), g(g_), coloredVerts(coloredVerts_), scale(scale_) {}

		template<class Vertex>
		void operator()(std::ostream &out, const Vertex &v) const {
			const int dim = g[v].pos.size();
			const auto& pos = g[v].pos;
			std::stringstream posStr;
			for (int i = 0; i < dim -1; ++i) {
				posStr << scale * pos(i);
			    posStr << ",";
			}
			posStr << scale * pos(dim -1);

			//Find out color of vertex
			bool isStart = v == start;
			bool isGoal = v == goal;
			bool isOnPath = isStart || isGoal || std::find(coloredVerts.begin(), coloredVerts.end(), v) != coloredVerts.end();

			const auto blue  = "#644AFF";
			const auto red   = "#EB4158";
			const auto green = "#8DFF94";
			const auto gray  = "#91A6BA";
			const auto black = "#000000";
			const std::string& color = isStart ? blue : (isGoal ? red : (isOnPath ? green : (g[v].visited ? black : gray)));
			//start=blue, goal=red, path=green, visited=black, not visited=gray

			out << "[pos=\""
			    << posStr.str()
			    << "\", fontsize=\"11\""
				<< ", color=\"" << color << "\"]";
		}

	private:
		Vertex& start, goal;
		Graph& g;
		std::list<Vertex>& coloredVerts;
		double scale = 1.;
	};

	/**
	 * Same as VertexWriter but for edges
	 */
	class EdgeWriter {
	public:
	    EdgeWriter(Graph& g_, std::list<Edge>& coloredEdges_)
                : g(g_), coloredEdges(coloredEdges_), weights(boost::get(boost::edge_weight, g)) {}
		template<class Edge>
		void operator()(std::ostream &out, const Edge &e) const {

            //Find out color of edge
            bool isOnPath = std::find(coloredEdges.begin(), coloredEdges.end(), e) != coloredEdges.end();
            const auto green = "#8DFF94";
            const auto gray  = "#91A6BA";
            const std::string& color = isOnPath ? green : gray;

			out << "[fontsize=\"11\""
			    << ", color=\"" << color << "\", weight=\"" << weights[e] << "\"]";
		}
	private:
	    std::list<Edge>& coloredEdges;
	    Graph& g;
	    WeightMap weights;
	};

	/**
	 * Prints a graph to a graphviz file
	 * @param shortestPath list of vertices on shortest path
	 */
	template <typename Graph>
	void PrintGraph(std::string filename, Graph &g, Vertex& start, Vertex& goal,
			std::list<Vertex>& shortestPath, std::list<Edge>& shortestPathEdges) {
		std::ofstream dotfile;
		dotfile.open(filename);
		boost::write_graphviz(dotfile, g,
		                      VertexWriter<std::vector<VertexData>>(start, goal, g, shortestPath, 200.),
		                      EdgeWriter(g, shortestPathEdges), GraphWriter());
	}
}