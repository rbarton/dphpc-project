#pragma once
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/astar_search.hpp>
#include <boost/graph/graphviz.hpp>
#include <Eigen/Dense>
#include <utility>

#include "graph.h"
#include "ioconfig.h"

using AStarWeightMap = boost::property_map<Graph, EdgeData>;
using AStarWeightVec = boost::vector_property_map<doubleT>;

namespace astar{
	struct foundGoal{}; // exception for termination

	class AStarNorm : public boost::astar_heuristic<Graph, doubleT> {
    public:
        AStarNorm(Norm& norm_, Vertex& goal_, const Graph& graph_)
                : norm(norm_), goal(goal_), g(graph_) {}

        inline doubleT operator()(Vertex u) {
            return norm(g[goal],g[u]);
        }

    private:
	    Norm& norm;
	    Vertex& goal;
        const Graph& g;
    };


	/**
	 * Visitor that terminates when we find the goal, used by boost astar
	 */
    class AstarGoalVisitor : public boost::default_astar_visitor {
    public:
        AstarGoalVisitor(Vertex& goal) : goal(goal) {};

        void examine_vertex(Vertex& u, const Graph &g) {
            if (u == goal)
                throw foundGoal();
        }

    private:
        Vertex goal;
    };

    bool AStarTest(const Graph& g, Vertex& start, Vertex& goal, Norm norm);
	bool AStarBench(const Graph& g, Vertex& start, Vertex& goal, Norm norm, std::vector<Vertex>& pred, std::vector<doubleT>& dist);

}   // namespace astar