#pragma once
#include <string>
#include <vector>
#include "graph.h"
#include <math.h>

/**
 * Reading run.config files to cpp struct
 */
namespace ioconfig {
	enum RunMode {
		debug = 0, bench = 1
	};
	enum SPAlgorithm {
		AlgoFringeSeq = 0, AlgoFringeOMP = 1, AlgoFringeOMPfs = 2, AlgoFringeMPI = 3,
		AlgoAStar = 4, AlgoDijkstra = 5
	};
	enum GraphType {
		test = 0, grid = 1, random = 2, road=3
	};
	enum NormType {
		LInf = 0, L1 = 1, L2 = 2, L2Sqr = 3, Haversine = 4
	};
	enum RandomGraphType {
		randomUniform = 0, randomClusters = 1, specialGraph = 2
	};
	enum RoadGraphType{
	    Luxembourg = 0, Asia = 1, Europe = 2
	};



	/**
	 * Stores all the values imported from the run.config file
	 * Only reads text inside the C++ Params section.
	 */
	struct RunConfig {
	    static RunConfig* get;
		RunConfig();

		unsignedT numThreadsProcesses = 1;
		unsignedT numMPIProcs = 1;
		unsignedT numOMPThreads = 1;

		RunMode runMode = debug;
		SPAlgorithm testAlgorithm = AlgoFringeSeq;
		doubleT costRelaxation = 1.;

		GraphType graphType = random;
		double avgVertDeg = 4;
		std::vector<unsignedT> gridDimensions{3, 3};
		unsignedT numVerts = 100;
		double gridSize = 50.0;
		RandomGraphType randomGraphType = randomClusters;
		NormType normType = L2;

		int benchIterations = 5;
		std::string benchFile = "bench.csv";

		double seed = 0;
		bool useRandomStartGoal = false;
		std::string configFile = "";

        RoadGraphType roadGraphType = Luxembourg;

        //Not really part of the config, but for convenience
        std::ostream* csvOut = &std::cout;

		/**
		 * Set the random Seed, use newSeed = 0 to use the current time
		 */
		template<typename T>
		void setSeed(T newSeed) {
			seed = (newSeed == 0 && numMPIProcs < 2) ? time(0) : newSeed;
		}
	};

	/**
	 * Reads the run.config into a struct RunConfig
	 * Detects -c=alternate.config in the argv to override the default config
	 */
	RunConfig ReadConfig(int argc, char **argv, const std::string &defaultFileName);

	/**
	 * Selects Norm from config
	 * @param normType type of norm read from config
	 * @return norm as a functor
	 */
	Norm GetNorm(NormType normType);

	/**
	 * Selects a Heuristic from config
	 * Currently only uses the normType
	 * @param normType from runConfig
	 * @param goalPos Eigen Vector of position
	 * @return A Heuristic std::function
	 */
	Heuristic GetHeuristic(NormType normType, Position &goalPos);


    inline doubleT DegreeToRadian(doubleT angle)
    {
        return M_PI * angle / 180.0;
    }
}
