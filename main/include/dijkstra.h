#pragma once
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <boost/graph/distributed/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
//#include <boost/graph/use_mpi.hpp>
//#include <boost/graph/distributed/mpi_process_group.hpp>

#include <Eigen/Dense>
#include <utility>


#include "graph.h"
#include "ioconfig.h"

//using DistGraph = boost::adjacency_list<boost::listS,boost::distributedS<boost::graph::distributed::mpi_process_group, boost::vecS>,
//        boost::undirectedS,VertexData,boost::property<boost::edge_weight_t, doubleT>>;
//using DistGTraits = boost::graph_traits<DistGraph>;
//using DistVertex = DistGraph::vertex_descriptor;
//using DistEdge   = DistGraph::edge_descriptor;
//using DistVertexIt = DistGTraits::vertex_iterator;
//using DistVertexAdjIt = DistGTraits::adjacency_iterator;
//using DistEdgeIt   = DistGTraits::edge_iterator;
//using DistEdgeAdjIt   = DistGTraits::out_edge_iterator;
//using DistWeightMap = boost::property_map<DistGraph, boost::edge_weight_t>::type;



namespace dijkstra{

    struct foundGoal{}; // exception for termination

    /**
     * Visitor that terminates when we find the goal, used by boost astar
     */
    class DijkstraGoalVisitor : boost::default_bfs_visitor{
    protected:
        Vertex goal;
    public:
        DijkstraGoalVisitor(Vertex destination_vertex_l)
                : goal(destination_vertex_l) {};

        void initialize_vertex(const Vertex &s, const Graph &g) const {}
        void discover_vertex(const Vertex &s, const Graph &g) const {}
        void examine_vertex(const Vertex &s, const Graph &g) const {}
        void examine_edge(const Edge &e, const Graph &g) const {}
        void edge_relaxed(const Edge &e, const Graph &g) const {}
        void edge_not_relaxed(const Edge &e, const Graph &g) const {}
        void finish_vertex(const Vertex &s, const Graph &g) const {
            if (goal == s)
                throw foundGoal();
        }
    };





    bool DijkstraBench(const Graph& g, Vertex& start, Vertex& goal, std::vector<Vertex>& pred, std::vector<doubleT>& dist);
    bool DijkstraTest(const Graph& g, Vertex& start, Vertex& goal);

}
