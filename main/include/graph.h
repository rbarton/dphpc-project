#pragma once
#include <limits>
#include <fstream>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <Eigen/Dense>

/**
 * This file includes typedefs for boost graphs, defines data stored per edge/vertex, heuristics, classes required for astar
 * how to print the graph and shortest path to a dot file (graphviz)
 */


const int posDim=2;
using doubleT = float;
using unsignedT = unsigned int;

struct VertexData;
struct EdgeData;

using UGraph = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, VertexData, boost::property<boost::edge_weight_t, doubleT>>;
//using DGraph = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS  , VertexData, EdgeData>;
using Graph  = UGraph;
using GTraits = boost::graph_traits<Graph>;
using Vertex = Graph::vertex_descriptor;
using Edge   = Graph::edge_descriptor;
using VertexIt = GTraits::vertex_iterator;
using VertexAdjIt = GTraits::adjacency_iterator;
using EdgeIt   = GTraits::edge_iterator;
using EdgeAdjIt   = GTraits::out_edge_iterator;
using WeightMap = boost::property_map<Graph, boost::edge_weight_t>::type;
using Position = Eigen::Matrix<doubleT, posDim, 1>;
using Heuristic = std::function<doubleT(const VertexData&)>;
using Norm = std::function<doubleT(const VertexData&, const VertexData&)>;
using Adjacency = std::pair<unsignedT, unsignedT>;
using AdjacencyVec = std::vector<Adjacency>;
//using Edge = std::pair<int, int>;
template<typename T>
constexpr int elementsPerCacheBlock(){
	return 64 / sizeof(T);
};
const auto verticesPerCacheBlock = elementsPerCacheBlock<Vertex>();

using AStarGraph = boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS, boost::no_property, boost::property<boost::edge_weight_t, doubleT>>;
//using AStarWeightMap = boost::property_map<Graph, doubleT>;
using WeightVec = std::vector<doubleT>;
using DegreeMap = std::map<unsignedT,unsignedT>;

//AStar types
struct vertex_hash : std::unary_function<Vertex, std::size_t> {
	std::size_t operator()(Vertex const& u) const {
		std::size_t seed = 0;
		boost::hash_combine(seed, u);
		return seed;
	}
};

using vertex_set = boost::unordered_set<Vertex, vertex_hash>;
using pred_map = boost::unordered_map<Vertex,Vertex,vertex_hash>;
using dist_map = boost::unordered_map<Vertex, doubleT,vertex_hash>;

/**
 * Stores for one vertex
 * @tparam posDim compile time dimension of Eigen::VectorXd for the position
 * @tparam Precision float or double, used by pos, fgh
 */
struct VertexData {
	VertexData(): pos{Position::Zero()} {
		omp_init_lock(&lock);
	}
	VertexData(Position pos_) : pos(std::move(pos_)) {
		omp_init_lock(&lock);
	}
	VertexData(const VertexData& other) {
		pos = other.pos;
	    h = other.h;
	    g = other.g;
	    f = other.f;
	    visited = other.visited;
	    parent = other.parent;
		omp_init_lock(&lock);
	}

	void Reset (){
		h = std::numeric_limits<doubleT>::infinity();
		g = std::numeric_limits<doubleT>::infinity();
		f = std::numeric_limits<doubleT>::infinity();
		visited = false;
		parent = -1;
		omp_destroy_lock(&lock);
		omp_init_lock(&lock);
	}

	Position pos;
	doubleT h = std::numeric_limits<doubleT>::infinity();
	doubleT g = std::numeric_limits<doubleT>::infinity();
	doubleT f = std::numeric_limits<doubleT>::infinity();
	bool visited = false;
    Vertex parent = -1;
	omp_lock_t lock;
};

void reconstructPathViaParent(Graph& g, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges);
void reconstructPathViaF(Graph& g, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges);
void reconstructPathAstar(Graph& g, std::vector<Vertex>& p, const Vertex& start, const Vertex& goal, std::list<Vertex>& path, std::list<Edge>& pathEdges);



