#pragma once
#include <vector>
#include <list>
#include <limits>
#include <algorithm>
#include "graph.h"

namespace fringe {
	bool FringeSearchSeq(Graph &g, const Vertex &start, const Vertex &goal, const doubleT costRelaxation, const Heuristic& heuristic);
	bool FringeSearchOMP(Graph &g, const Vertex &start, const Vertex &goal, const doubleT costRelaxation, const Heuristic& heuristic);
	bool FringeSearchOMPfs(Graph &g, const Vertex &start, const Vertex &goal, const doubleT costRelaxation, const Heuristic& heuristic);
	bool FringeSearchMPI(Graph &g, const Vertex &start, const Vertex &goal, const doubleT costRelaxation, const Heuristic& heuristic);
}
