#include <iostream>
#include <fstream>
#include <chrono>
#include <Eigen/Dense>
#include <mpi.h>
#include <omp.h>
#ifdef CUDA_ENABLED
    #include "cuda_kernels.h"
#endif
#include "ioconfig.h"
#include "iograph.h"
#include "graph.h"
#include "fringe.h"
#include "astar.h"
#include "dijkstra.h"

#include <omp.h>
#include <mpi.h>
#include <boost/graph/random.hpp>
#include <boost/random.hpp>
#include <boost/graph/erdos_renyi_generator.hpp>
#include <random>

using namespace std::chrono;

void TestFringe(const std::function<bool()>& lambda, Graph& g, Vertex& start, Vertex& goal, const bool useReconstructionViaParent);
void TestAstar(Norm norm,const Graph& g, Vertex& start, Vertex& goal);
void TestDijkstra(const Graph& g, Vertex& start, Vertex& goal);

template<typename Lambda>
void Bench(Graph& g, Lambda lambda, std::string csvLabel, ioconfig::RunConfig& runConfig, Vertex goal);
inline bool FileExist (const std::string& name) {
	std::ifstream f(name.c_str());
	return f.good();
}


int main(int argc, char **argv) {

	MPI_Init(&argc, &argv);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	ioconfig::RunConfig runConfig = ioconfig::ReadConfig(argc, argv, "run.config");         //--- Import run.config
	std::mt19937 gen(runConfig.seed);
	std::cout << "VertexData: " << sizeof(VertexData) << "B, EdgeData: " << sizeof(doubleT) << "B, Adj: "
	          << sizeof(std::pair<unsignedT, unsignedT>) << "Bytes" << std::endl
	          << (sizeof(VertexData) + 2 * (sizeof(doubleT) + sizeof(std::pair<unsignedT, unsignedT>))) *
	             runConfig.gridDimensions[0] * runConfig.gridDimensions[1] / 1024. / 1024. / 1024. << "GB lower bound" << std::endl;

	const Norm norm = GetNorm(runConfig.normType);
	Graph g;
	if (runConfig.graphType == ioconfig::GraphType::grid) {                                //--- Generate graph
		iograph::GenerateGridGraph(g, runConfig, gen);
	} else if (runConfig.graphType == ioconfig::GraphType::random) {
		iograph::GenerateRandomGraph(g, runConfig, gen);
	} else if (runConfig.graphType == ioconfig::GraphType::road){
	    iograph::GenerateRoadGraph(g, runConfig);
	}
	std::cout << "Generated Graph." << std::endl;

	Vertex start, goal;
	if(runConfig.useRandomStartGoal) {
		start = random_vertex(g, gen);                                                   //--- Setup search
		goal = random_vertex(g, gen);
	} else {
		start = 0;
		goal = num_vertices(g) - 1;
	}
	std::cout << "Start vertex: " << start << std::endl;
	std::cout << "Goal vertex : " << goal << std::endl;


	//Get graph properties
//	iograph::WeightInfo(g);
//	iograph::DegreeInfo(g);

	const Heuristic heuristic = GetHeuristic(runConfig.normType, g[goal].pos);

	// ---- Execute Search ----
	const auto &fringeSeq = [&]()->bool {
        // dummy variables to include path reconstruction in bench
        std::list<Vertex> shortestPath;
        std::list<Edge> shortestPathEdges;

        bool goalFound = fringe::FringeSearchSeq(g, start, goal, runConfig.costRelaxation, heuristic);
        reconstructPathViaParent(g, start, goal, shortestPath, shortestPathEdges);
		*ioconfig::RunConfig::get->csvOut << ", " << std::setw(12) << g[goal].g;
        return goalFound;
	};
	const auto &fringeOMP = [&]()->bool {
        // dummy variables to include path reconstruction in bench
        std::list<Vertex> shortestPath;
        std::list<Edge> shortestPathEdges;

		bool goalFound = fringe::FringeSearchOMP(g, start, goal, runConfig.costRelaxation, heuristic);
        reconstructPathViaParent(g, start, goal, shortestPath, shortestPathEdges);
		*ioconfig::RunConfig::get->csvOut << ", " << std::setw(12) << g[goal].g;
		return goalFound;
	};
	const auto &fringeOMPfs = [&]() {
        // dummy variables to include path reconstruction in bench
        std::list<Vertex> shortestPath;
        std::list<Edge> shortestPathEdges;

		bool goalFound = fringe::FringeSearchOMPfs(g, start, goal, runConfig.costRelaxation, heuristic);
        reconstructPathViaParent(g, start, goal, shortestPath, shortestPathEdges);
		*ioconfig::RunConfig::get->csvOut << ", " << std::setw(12) << g[goal].g;
		return goalFound;
	};
	const auto &fringeMPI = [&]() {
        // dummy variables to include path reconstruction in bench
        std::list<Vertex> shortestPath;
        std::list<Edge> shortestPathEdges;

		bool goalFound = fringe::FringeSearchMPI(g, start, goal, runConfig.costRelaxation, heuristic);
		if (rank == 0) {
			reconstructPathViaF(g, start, goal, shortestPath, shortestPathEdges);
			*ioconfig::RunConfig::get->csvOut << ", " << std::setw(12) << g[goal].g; //path length
		}
		return goalFound;
	};
	const auto &aStar = [&]() {
		std::list<Vertex> shortestPath;
		std::list<Edge> shortestPathEdges;
		std::vector<Vertex> pred(num_vertices(g));
		std::vector<doubleT> dist(num_vertices(g));

		bool goalFound = astar::AStarBench(g, start, goal, norm, pred, dist);
		reconstructPathAstar(g, pred, start, goal, shortestPath, shortestPathEdges);
		*ioconfig::RunConfig::get->csvOut << ",        0, " << std::setw(12) << dist[goal];
		return goalFound;
	};
	const auto &dijkstra = [&]() {
        std::list<Vertex> shortestPath;
        std::list<Edge> shortestPathEdges;
        std::vector<Vertex> pred(num_vertices(g));
        std::vector<doubleT> dist(num_vertices(g));

        bool goalFound = dijkstra::DijkstraBench(g, start, goal, pred, dist);
        reconstructPathAstar(g, pred, start, goal, shortestPath, shortestPathEdges);
        *ioconfig::RunConfig::get->csvOut << ",        0, " << std::setw(12) << dist[goal];
        return goalFound;
	};

	MPI_Barrier(MPI_COMM_WORLD);
	if (runConfig.runMode == ioconfig::RunMode::debug) {                  //--- Debug Mode
		if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoFringeSeq) {
            TestFringe(fringeSeq, g, start, goal, true);
		}
        else if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoFringeOMP) {
            TestFringe(fringeOMP, g, start, goal, true);
        }
		else if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoFringeOMPfs) {
            TestFringe(fringeOMPfs, g, start, goal, true);
        }
		else if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoFringeMPI) {
            TestFringe(fringeMPI, g, start, goal, false);
        }
		else if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoAStar)
			TestAstar(norm, g, start, goal);
		else if (runConfig.testAlgorithm == ioconfig::SPAlgorithm::AlgoDijkstra)
		    TestFringe(dijkstra, g, start, goal, false);
//		    TestDijkstra(g, start, goal);
	} else if (runConfig.runMode == ioconfig::RunMode::bench) {            //--- Bench Mode
		if(runConfig.numThreadsProcesses == 1){
			Bench(g, fringeSeq, "FringeSeq", runConfig, goal);
		    Bench(g, aStar, "Astar", runConfig, goal);
		}
		if(runConfig.numMPIProcs == 1) {
			Bench(g, fringeOMP, "FringeOMP", runConfig, goal);
			Bench(g, fringeOMPfs, "FringeOMPfs", runConfig, goal);
		}
		Bench(g, fringeMPI, std::string("FringeMPI").append(std::to_string(runConfig.numMPIProcs).append("p")), runConfig, goal);
	}

	MPI_Finalize();
	return 0;
}

/**
 * Test the runtime for custom fringe search
 */
void TestFringe(const std::function<bool()>& lambda, Graph& g, Vertex& start, Vertex& goal, const bool useReconstructionViaParent) {
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if(rank != 0){
		lambda();
		return;
	}

	//--- Extract Shortest path & Print
	std::list<Vertex> shortestPath;
	std::list<Edge> shortestPathEdges;

	auto startTime = high_resolution_clock::now();
	bool goalFound = lambda();
	if (useReconstructionViaParent) {
        reconstructPathViaParent(g, start, goal, shortestPath, shortestPathEdges);
    }
	else {
        reconstructPathViaF(g, start, goal, shortestPath, shortestPathEdges);
	}
	auto endTime = high_resolution_clock::now();

	double runtime = duration_cast<duration<double>>(endTime - startTime).count();
	std::cout << "Found Goal    : " << goalFound << std::endl;
	std::cout << "Fringe runtime: " << runtime << "s\n";

//    auto spi = shortestPath.begin();
//    for (++spi; spi != shortestPath.end(); ++spi)
//        std::cout << " -> " << *spi;

	std::cout << std::endl << "Total travel time: " << g[goal].g << std::endl;

	if(boost::num_vertices(g) <= 10000) {
		std::cout << "Printing Graph..." << std::endl;
		iograph::PrintGraph("graph.dot", g, start, goal, shortestPath, shortestPathEdges);
		std::cout << "Done." << std::endl;
	}
}

/**
 * Test the runtime of AStar search
 */
void TestAstar(Norm norm, const Graph& g, Vertex& start, Vertex& goal){
    auto startTime = high_resolution_clock::now();
    bool goalFound = astar::AStarTest(g, start, goal, norm);
    auto endTime = high_resolution_clock::now();

    double runtime = duration_cast<duration<double>>(endTime - startTime).count();
    std::cout << "Found Goal    : " << goalFound << std::endl;
    std::cout << "AStar runtime: " << runtime << "s (excl. reconstruction)\n";

    std::list<Vertex> shortestPath;
    std::list<Edge> shortestPathEdges;
//	reconstructPathAstar(g, pred, start, goal, shortestPath, shortestPathEdges);
}

void TestDijkstra(const Graph& g, Vertex& start, Vertex& goal){
    auto startTime = high_resolution_clock::now();
    bool goalFound = dijkstra::DijkstraTest(g, start, goal);
    auto endTime = high_resolution_clock::now();

    double runtime = duration_cast<duration<double>>(endTime - startTime).count();
    std::cout << "Found Goal    : " << goalFound << std::endl;
    std::cout << "Dijkstra runtime: " << runtime << "s (excl. reconstruction)\n";

    std::list<Vertex> shortestPath;
    std::list<Edge> shortestPathEdges;
}

/**
 * Benchmark runtime for any lambda
 * @tparam Lambda Function with no parameters
 * @param csvLabel label for method
 */
template<typename Lambda>
void Bench(Graph& g, const Lambda lambda, const std::string csvLabel, ioconfig::RunConfig& runConfig, Vertex goal){
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank != 0){
		for (int i = 0; i < runConfig.benchIterations; ++i) {
			lambda();

			#pragma omp parallel for
			for (Vertex v = 0; v < boost::num_vertices(g); ++v) {
				g[v].Reset();
			}
		}
		return;
	}

	// ------------- Rank 0 only ------------------------

	//Print output into a csv, prints to cout if it fails
	bool useCout = false;
	bool fileExists = FileExist(runConfig.benchFile);

	std::ofstream benchOut;                                         //Try to open a stream to the file,
	benchOut.open(runConfig.benchFile, std::fstream::app);
	if (!benchOut.good()) {
		std::cerr << "Failed to open bench file: " << runConfig.benchFile << ", printing to std::cout instead";
		useCout = true;
	}
	else if(!fileExists)    //write header if the file is new
		benchOut << "NumCores,NumMPIProcs,NumOMPThreads,Method,CostRelaxation,AvgFringeSize,PathLength,Runtime" << std::endl; //Note! no spaces or you may get a KeyError in pandas

	std::ostream& out = useCout ? std::cout : benchOut;
	ioconfig::RunConfig::get->csvOut = &out;
	out << std::scientific;

	std::cout << csvLabel;
	std::cout.flush();
	time_point<high_resolution_clock> startTime, endTime;
	double runtime;
	doubleT pathLength = std::numeric_limits<doubleT>::infinity();
	for (int i = 0; i < runConfig.benchIterations; ++i) {                               //--- Time several times
		out << std::setw(5) << runConfig.numThreadsProcesses;
		out << ", " << std::setw(5) << runConfig.numMPIProcs;
		out << ", " << std::setw(5) << runConfig.numOMPThreads;
		out << ", " << std::setw(12) << csvLabel;
		out << ", " << std::setw(5) << runConfig.costRelaxation;

		startTime = high_resolution_clock::now();       //Note: avgFringeSize and path length are printed inside lambda!
        bool goalFound = lambda();
		endTime = high_resolution_clock::now();

		runtime = duration_cast<duration<double>>(endTime - startTime).count();        //--- Print runtime in a nice format
		out << ", " << std::setw(12) << runtime << std::endl;

		std::cout << ".";
		std::cout.flush();

		// reset values before next run
		#pragma omp parallel for
		for (Vertex v = 0; v < boost::num_vertices(g); ++v) {
		    g[v].Reset();
		}
	}
	std::cout << std::endl;
}
