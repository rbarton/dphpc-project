#include "iograph.h"
#include <random>
#include "ioconfig.h"
#include <fstream>
namespace iograph {

	void GenerateGridGraph(Graph& g, ioconfig::RunConfig runConfig, std::mt19937& gen){
		const auto& dims = runConfig.gridDimensions;
		const auto& norm = ioconfig::GetNorm(runConfig.normType);

		if(dims.size() != posDim) {
			std::string error =	"Your Position Dimensions don't match, graph.h:posDim=" + std::to_string(posDim);
			error +=  ", run.config:gridDimensions=" + std::to_string(dims.size()) + ". GenerateGridGraph()";
			throw std::runtime_error(error);
		}

		//Find number verts/edges to preallocate vectors
		unsignedT dim = dims.size();
		unsignedT numVerts  = 1;
		unsignedT numEdges = 1;
		for (const auto& it : dims) {
			numVerts *= it;
			numEdges *= it -1;
		}
		g.m_vertices.reserve(numVerts);

		std::normal_distribution<doubleT> gaussian(0, 0.25f);
		//Loop over all vertices and extract pos, adjacency from index using / and %
		for (unsignedT i = 0; i < numVerts; ++i) {
			//get position in grid
			Position pos(dim);
			unsignedT rest = i;
			for (int j = 0; j < dim; ++j) {
				pos(j) = rest % dims[j] + gaussian(gen);
				rest /= dims[j];
			}
			add_vertex({pos}, g);

			//Add edges, each vertex only adds edges in direction from 0 to other corner of grid
			//Do this by introducing the shift, which iterates through all offsets to get adjacent vertices
			unsignedT shift = 1;
			unsignedT iInDim = i;   //tells us where in this dim we are, used to detect if we are on edge of grid
		}
		std::cout << "Added verts\n";

		//only add weight when all vertices exist
		for (Vertex i = 0; i < numVerts; ++i) {
			unsignedT shift = 1;  //reset vars!
			unsignedT iInDim = i;
			for (unsignedT j = 0; j < dim; ++j) {
				if (i + shift >= numVerts)
					break;

				if ((iInDim + 1) % dims[j] != 0) { //if not on edge of this dim
					add_edge(i, i + shift, {norm(g[i], g[i + shift])}, g);
				}
				shift *= dims[j];
				iInDim /= dims[j];
			}
		}
		std::cout << "Added edges\n";

	}

	void GenerateRandomGraph(Graph& g, ioconfig::RunConfig& runConfig, std::mt19937 & rng){
		//set some default for graphFraction if input is invalid
		if (runConfig.avgVertDeg < 1) {
			std::string error = "runConfig.vertDeg >= 1 must be satisfied in GenerateRandomGraph(), avgVertDeg=";
			error += std::to_string(runConfig.avgVertDeg);
			throw std::runtime_error(error);
		}
		const auto& norm = ioconfig::GetNorm(runConfig.normType);
		const auto numberEdges = static_cast<unsignedT>(runConfig.numVerts * runConfig.avgVertDeg / 2);

		//resize vectors in advance
		g.m_vertices.reserve(runConfig.numVerts);
		//don't need to reserve edges as its a std::list

		auto randPos = [&](std::uniform_real_distribution<doubleT> randPosDistr) {
			return Position::Zero().unaryExpr([&](doubleT dummy) {
				return randPosDistr(rng);
			});
		};

		if (runConfig.randomGraphType == ioconfig::randomUniform)
		{
			std::uniform_real_distribution<doubleT> randPosDistr(0.0, runConfig.gridSize);
			std::uniform_int_distribution<unsignedT> randVertex(0, runConfig.numVerts - 1);
			std::normal_distribution<doubleT> gaussian(1., 0.25);

			add_vertex({randPos(randPosDistr)}, g);

			//Add vertices and edges to ensure connectedness
			for (unsignedT i = 1; i < runConfig.numVerts; ++i) {
				add_vertex({randPos(randPosDistr)}, g);
				//make sure all vertices are reachable
				add_edge(i - 1, i, {norm(g[i-1],g[i])}, g);
			}

			//Add rest of edges randomly

			for (unsignedT i = runConfig.numVerts; i < numberEdges; ++i) {
			    unsignedT v1 = randVertex(rng);
			    unsignedT v2 = randVertex(rng);
			    doubleT dist = norm(g[v1], g[v2]);

			    add_edge(v1, v2, {dist}, g);
			}
		}
		else if (runConfig.randomGraphType == ioconfig::randomClusters)
		{
			std::uniform_real_distribution<doubleT> randWeight(0., 100.);
			std::uniform_int_distribution<unsignedT> randVertex(0, runConfig.numVerts - 1);
			std::uniform_real_distribution<doubleT> real1(0, 50);
			std::uniform_real_distribution<doubleT> real2(50, 100);

			//Add vertices for first cluster
			add_vertex({randPos(real1)}, g);
			for (unsignedT i = 1; i < runConfig.numVerts / 2; ++i) {
				add_vertex({randPos(real1)}, g);
				//make sure all vertices within one cluster are reachable
				add_edge(i - 1, i, {norm(g[i-1],g[i])}, g);
			}

			//Add second cluster
			for (unsignedT i = runConfig.numVerts / 2; i < runConfig.numVerts; ++i) {
				add_vertex({randPos(real2)}, g);
				//make sure all vertices within one cluster are reachable
				add_edge(i - 1, i, {randWeight(rng)}, g);
			}
			add_edge(0, runConfig.numVerts / 2, {randWeight(rng)}, g); //make sure one cluster can reach the next


            for (unsignedT i = runConfig.numVerts; i < numberEdges; ++i) {
                unsignedT v1 = randVertex(rng);
                unsignedT v2 = randVertex(rng);
                doubleT dist = norm(g[v1], g[v2]);
                add_edge(v1, v2, {dist}, g);
            }
		}
		else if (runConfig.randomGraphType == ioconfig::specialGraph)
		{
			std::uniform_real_distribution<doubleT> real(0.0, runConfig.gridSize);
			std::uniform_real_distribution<doubleT> randWeight(0., 100.);
			std::uniform_int_distribution<unsignedT> randVertex(0, runConfig.numVerts - 1);

			add_vertex({randPos(real)}, g);
			unsignedT i = 0;
			while (i < runConfig.numVerts) {
				Position pos = randPos(real);
				if (pos.norm() < runConfig.gridSize / 3) {
					Vertex v = add_vertex(pos, g);
					//make sure all vertices are reachable
					add_edge(i - 1, i, {norm(g[i-1],g[i])}, g);
					i++;
				}
			}

			//Add edges randomly
            bool toofar;
            for (unsignedT i = runConfig.numVerts; i < numberEdges; ++i) {
                toofar = true;
                while(toofar) {
                    Vertex v1 = randVertex(rng);
                    Vertex v2 = randVertex(rng);
                    doubleT dist = norm(g[v1], g[v2]);
                    if (dist < runConfig.gridSize/std::log(runConfig.numVerts)) {
                        add_edge(v1, v2, {dist}, g);
                        toofar = false;
                    }
                }
            }
		}
	}

    void GenerateRoadGraph(Graph& g, ioconfig::RunConfig& runConfig) {
        std::string rdMatFile, rdCoordsFile;
        if (runConfig.roadGraphType == 0) {
            rdMatFile = "../../roadGraphs/luxembourg_osm/luxembourg_osm.mtx";
            rdCoordsFile = "../../roadGraphs/luxembourg_osm/luxembourg_osm_coord.mtx";
//            rdMatFile = "../roadGraphs/luxembourg_osm/luxembourg_osm.mtx";
//            rdCoordsFile = "../roadGraphs/luxembourg_osm/luxembourg_osm_coord.mtx";
        } else if (runConfig.roadGraphType == 1) {
	        rdMatFile = "../../roadGraphs/asia_osm/asia_osm.mtx";
	        rdCoordsFile = "../../roadGraphs/asia_osm/asia_osm_coord.mtx";
//	        rdMatFile = "../roadGraphs/asia_osm/asia_osm.mtx";
//	        rdCoordsFile = "../roadGraphs/asia_osm/asia_osm_coord.mtx";
        } else if (runConfig.roadGraphType == 2) {
            rdMatFile = "../../roadGraphs/europe_osm/europe_osm.mtx";
            rdCoordsFile = "../../roadGraphs/europe_osm/europe_osm_coord.mtx";
//            rdMatFile = "../roadGraphs/europe_osm/europe_osm.mtx";
//            rdCoordsFile = "../roadGraphs/europe_osm/europe_osm_coord.mtx";
        }
        const auto &norm = ioconfig::GetNorm(runConfig.normType);
        std::ifstream rdMat(rdMatFile);
        std::ifstream rdCoords(rdCoordsFile);
        if (!rdMat.is_open()) {
            std::cout << "failed to open " << rdMatFile << '\n';
        } else if (!rdCoords.is_open()) {
            std::cout << "failed to open" << rdCoordsFile << '\n';
        } else {
            unsignedT numEdges, numVertices;
            rdMat >> numVertices >> numEdges;
            g.m_vertices.reserve(numVertices);
            doubleT x, y;
            unsignedT u, v;
            while (rdCoords >> x >> y) {
                Position pos(2);
                pos(0) = x/100000;
                pos(1) = y/100000;
                add_vertex({pos}, g);
            }
            for (unsignedT i = 0; i < numEdges; i++) {
                rdMat >> u >> v;
                u--;
                v--;
                add_edge(u, v, {norm(g[u], g[v])}, g);
            }
        }
    }

    //------------ Helper Functions --------------------------------------------------------------------------
    void WeightInfo(const Graph& g){
//	    WeightMap weightVec = boost::get(boost::edge_weight, g);
	    unsignedT numEdges = num_edges(g);
        doubleT weightSum = 0;
//        for (unsignedT i = 0; i< numEdges; i++)
//            weightSum += weightVec[i];
//        std::cout << "Max weight: " << *std::max_element(weightVec.begin(),weightVec.end()) << std::endl;
//        std::cout << "Index of max element: " << std::distance(weightVec.begin(), std::max_element(weightVec.begin(), weightVec.end()))<< std::endl;
//        std::cout << "Min weight: " << *std::min_element(std::begin(weightVec), std::end(weightVec)) << std::endl;
//        std::cout << "Index of min element: " << std::distance(weightVec.begin(), std::min_element(weightVec.begin(), weightVec.end())) << std::endl;
//        std::cout << "Sum of weights: " << weightSum << std::endl;
//        std::cout << "Avg weight: " << weightSum / doubleT(numEdges)<< std::endl;
	}

    const DegreeMap GetDegrees(const Graph& g){
        DegreeMap degreeMap;
        for(auto v : boost::make_iterator_range(vertices(g))){
            unsignedT vDegree = degree(v,g);
            if(degreeMap.find(vDegree)==degreeMap.end())
                degreeMap[vDegree]=1;
            else degreeMap[vDegree]++;
        }
        return degreeMap;
	}

	void DegreeInfo(const Graph& g){
//	    auto deg = GetDegrees(g);
//	    unsignedT degreeSum = 0;
//        for (const auto& [degree, count]: deg){ //Note: remove c++17 feature for clusters
//            degreeSum += degree*count;
//            std::cout << "degree: " << degree << "  count: " << count << std::endl;
//        }
//        std::cout << "Avg degree: " << degreeSum /doubleT(num_vertices(g)) << std::endl;
	}
}