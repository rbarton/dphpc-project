#include "dijkstra.h"


namespace dijkstra{



    bool DijkstraBench(const Graph& g, Vertex& start, Vertex& goal, std::vector<Vertex>& pred, std::vector<doubleT>& dist){
        bool goalFound = false;

        try{
            boost::dijkstra_shortest_paths(g, start,  weight_map(get(boost::edge_weight, g)).
                    predecessor_map(pred.data()).distance_map(dist.data()).
                    visitor(DijkstraGoalVisitor(goal)));
        }catch(foundGoal){
            goalFound = true;
        }
        return goalFound;
    }
    bool DijkstraTest(const Graph& g, Vertex& start, Vertex& goal){
        std::vector<Vertex> pred(num_vertices(g));
        std::vector<doubleT> dist(num_vertices(g));

        bool goalFound = false;

        try{
            boost::dijkstra_shortest_paths(g, start,  weight_map(get(boost::edge_weight, g)).
                    predecessor_map(pred.data()).distance_map(dist.data()).
                    visitor(DijkstraGoalVisitor(goal)));
        }catch(foundGoal){
            goalFound = true;
        }

        std::cout << std::endl << "Total travel time: " << dist[goal] << std::endl;
        return goalFound;
    }

}
