#include "fringe.h"
#include "ioconfig.h"
#include <algorithm>
#include <chrono>

namespace fringe {

    /**
	 * Private data for each omp thread
	 * Aligned to a cacheblock to prevent false-sharing
	 */
    struct alignas(128) ThreadData {
        unsignedT nextFreeIndex = 0;
        Vertex laterBlock[verticesPerCacheBlock];
    };

    /**
	  * Writes (partly filled) local block to shared vector
	  * Note! once a partly filled block is added you loose the alignment -> false sharing, only add these at the end
	  */
    template <class elemType>
    void OMP_updateVector(std::vector<elemType>& vector, elemType*& block, omp_lock_t& vectorLock, unsignedT& nextFreeLocal) {
        if (nextFreeLocal == 0)
            return;
        // Resize vector and then copy outside of critical section
        omp_set_lock(&vectorLock);
        const auto size = vector.size();
        vector.resize(size + nextFreeLocal);
        omp_unset_lock(&vectorLock);

        std::copy(block, block + nextFreeLocal, vector.begin() + size);
        nextFreeLocal = 0;
    }

    /**
     * False-sharing-free push_back for vector of type Vertex
     * If block is full write to shared vector and start "fresh" block
     */
    template <typename T>
    void OMP_PushBack(std::vector<T>& vector, T*& block, omp_lock_t& vectorLock, const T& v, unsignedT& nextFreeLocal) {
        block[nextFreeLocal++] = v;
        if (nextFreeLocal % elementsPerCacheBlock<T>() == 0) {  // if the private block is full, copy to shared vector
            OMP_updateVector(vector, block, vectorLock, nextFreeLocal);
        }
    }

	/**
	 * OpenMP Implementation of Fringe Search
	 * @param costRelaxation increment for fThres
	 * @return whether goal was found
	 */
	bool FringeSearchOMP(Graph& g, const Vertex& start, const Vertex& goal, const doubleT costRelaxation, const Heuristic& heuristic) {
		doubleT fThres;
		WeightMap weights = boost::get(boost::edge_weight, g);

		alignas(4096) std::vector<Vertex> now{}, later{};
		omp_lock_t laterLock;
		omp_init_lock(&laterLock);
        const int avgVertDeg = std::ceil(ioconfig::RunConfig::get->avgVertDeg);

		//log/bench variables
		int fringeSizeLog = 0;
		int fringeSizeLogCount = 0;

		now.push_back(start);
		g[start].g = 0;
		g[start].f = g[start].h = heuristic(g[start]);
		g[start].parent = start;

        doubleT fMin = std::numeric_limits<doubleT>::infinity();
		bool goalFound = false;

		do {
			// computing new threshold
			fThres = fMin + costRelaxation;
			fMin = std::numeric_limits<doubleT>::infinity();

			// --- Prepare Parallel ---
			// determine omp threads used for this iteration, not too many threads
			int activeThreads = std::min(omp_get_max_threads(), std::max(1, (int)now.size() / verticesPerCacheBlock +1));
			//TODO: how to choose this? (is actually indep. of cacheBlock size)

            // stores where each thread is inserting in the later block, zero padded for false sharing via 2D vector
            std::vector<ThreadData> threadData(activeThreads, ThreadData{});

            later.reserve(std::max<unsignedT>(now.size() * avgVertDeg, (activeThreads + 1) * verticesPerCacheBlock));

			// iterate in parallel over the now vector
            #pragma omp parallel for num_threads(activeThreads) reduction(min : fMin)
			for (unsignedT i = 0; i < now.size(); ++i) {
				if(goalFound) {// break when goal found (omp hack)
					i = now.size();
					continue;
				}

                unsignedT& nextFreeLocal = threadData[omp_get_thread_num()].nextFreeIndex;
                Vertex* laterBlock = threadData[omp_get_thread_num()].laterBlock;
			    Vertex& v = now[i];

                doubleT fVal;
                #pragma omp atomic read
                fVal = g[v].f;

				if (fVal > fThres) {  // skip edges over the f threshold
                    fMin = std::min(fVal, fMin);
                    OMP_PushBack(later, laterBlock, laterLock, v, nextFreeLocal);
				    continue;
				}

				if (v == goal) {
                    std::cout << "t" << omp_get_thread_num() << " found goal" << std::endl;
                    goalFound = true;
					continue;
				}

				#pragma omp atomic write
				g[v].visited = true;

                doubleT gVal;
                #pragma omp atomic read
                gVal = g[v].g;

				// Iterate over outgoing edges (better than iterating over neighbours as we can get vertices easily)
				EdgeAdjIt adjEdgeIt, adjEdgeItEnd;
				for (boost::tie(adjEdgeIt, adjEdgeItEnd) = boost::out_edges(v, g); adjEdgeIt != adjEdgeItEnd; ++adjEdgeIt) {
					Vertex neighbour = boost::target(*adjEdgeIt, g);
					bool skip;
                    #pragma atomic read
					skip = g[neighbour].visited;
					if (skip)
						continue;

					// lock vertex, check that no other thread is accessing this at the same time
					omp_set_lock(&g[neighbour].lock);

					// compute the travel cost leading up to neighbour(=M in paper)
					doubleT cost = gVal + weights[*adjEdgeIt];
					bool improved = cost < g[neighbour].g;

					if (improved) {
                        g[neighbour].h = heuristic(g[neighbour]);
                        g[neighbour].g = cost;
                        g[neighbour].f = cost + g[neighbour].h;
						fMin = std::min(g[neighbour].f, fMin);
                        g[neighbour].parent = v; // link back to
                        OMP_PushBack(later, laterBlock, laterLock, neighbour, nextFreeLocal);
                    }
					omp_unset_lock(&g[neighbour].lock);
				} // neighbour loop
			} // now loop - end of parallel

            // write remaining nodes to later, for each thread
            for (int t = 0; t < activeThreads; ++t) {
                unsignedT& nextFreeLocal = threadData[t].nextFreeIndex;
                Vertex* laterBlock = threadData[t].laterBlock;
                OMP_updateVector(later, laterBlock, laterLock, nextFreeLocal);
            }

			if(goalFound) {
                *ioconfig::RunConfig::get->csvOut << ", " << std::setw(8) << fringeSizeLog / fringeSizeLogCount;
                return true;
            }

            now.clear();
            std::swap(now, later);

            fringeSizeLog += now.size();
            ++fringeSizeLogCount;

		} while (!now.empty());

		// goal not found
		return false;
	}
} // namespace fringe
