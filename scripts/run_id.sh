#!/usr/bin/env bash

#we store the current run id/index in a file, for persistence & git
run_id_file="${out_root}.run_id_counter"     #be sure to keep absolute paths

#sets run_id from a value stored in a file, creates file if not found
#be sure to call this at the start of most functions, as the can be called independently
function get_run_id() {
  mkdir -p "${out_root}"
  if test -f "$run_id_file"; then
    run_id=$(head "${run_id_file}")
  else
    echo "0" > "${run_id_file}"
    run_id=0
  fi
  #echo ${run_id}
}

# Increments the id and updates, saves to file for persistence, finds id that does not exist
# Prevents having to add the run_id_file to git and overwriting issues
# Arg1: 'once' to only increment once, even if out dir exists
function increment_run_id() {
  get_run_id
  let "run_id++"
  if [[ "$1" != *"once"* ]]; then
    while [ -d "${out_root}${run_id}" ]; do       #keep incrementing until there the directory does not exist
      let "run_id++"
    done
  fi
  echo $run_id > "${run_id_file}"
}

#sets run_id and saves it to the file $run_id_file
function set_run_id() {
  if [[ -z $1 ]]; then
    echo "Usage: set_run_id <value>"
    get_run_id
  else
    run_id=$1
    mkdir -p "${out_dir}"
    echo $run_id > "${run_id_file}"
  fi
}