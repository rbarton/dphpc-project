#!/usr/bin/env bash

# This script loads the according Environment Modules for either euler or leonhard cluster
# Pass which modules to load through the first argument

if [[ "$0" == "$BASH_SOURCE" ]] ; then
  echo "Please run script with 'source' instead of ./"
  echo "    (modules must be loaded to current shell)"
  exit
fi

#note: MPI must be loaded *after* the cxx compiler,
module purge  #unload all modules first
if [[ "$1" == *"euler"* ]]; then
  module load new                                             #Core modules
  if [[ "${cmake_cxx_compiler}" == "intel" ]]; then           #Compiler
    module load intel/2018.1
  else
    module load gcc/6.3.0
  fi

  module load cmake/3.11.4 boost/1.62.0_py3 open_mpi/3.0.0    #All remaining modules

elif [[ "$1" == *"leo"* ]]; then
  module load StdEnv nano ninja
  if [[ "${cmake_cxx_compiler}" == "intel" ]]; then
    module load intel/18.0.1
  else
    module load gcc/8.2.0
  fi

  module load cmake/3.14.4 boost/1.63.0 cuda/10.1.243

  if [[ "${cmake_cxx_compiler}" == "intel" ]]; then           #Choose MPI available depending on compiler
    module load openmpi/2.1.0
  else
    module load openmpi/4.0.1
  fi
else
    echo "Usage: source load_modules.sh <cluster>"
    echo "       cluster = euler or leo"
    return 1
fi

module list

# To see which compilers and openmpi version are compatible
# Euler:    https://scicomp.ethz.ch/wiki/Euler_applications_and_libraries
# Leonhard: https://scicomp.ethz.ch/wiki/Leonhard_applications_and_libraries

# Note! wiki can be outdated search on euler with 'module avail gcc'
# On euler there are different categories legacy, new
# 'module load new' to also allow newer modules
# Leo, be sure to reload StdEnv to be able to load nano

# Boost
# euler: use the py3 versions to ensure boost is compatible with newer compiler versions
# leo  : use boost/1.63.0 not 1.69.0, cannot find graph otherwise
