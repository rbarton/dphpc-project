#!/bin/bash -l
#submit with bsub < submit_job.sh
#execute several commands as part of a script
#Note! environment variables will be pasted before submitting to bsub

#N=M*T = total threads
#Paramters for the bsub command
#BSUB -n ${total_threads}
#BSUB -R "span[ptile=${ptile_factor}] ${bsub_R_flags}"
#BSUB -N                    #receive email on finish
#BSUB -oo log.%J
#BSUB -W ${max_cluster_time}

hostname
lscpu

echo "MPI Processes : ${mpi_processes}"
echo "OpenMP Threads: ${OMP_NUM_THREADS}"
echo "Total Threads : ${total_threads}"
echo "GPUs per Node : ${gpus_per_node}"
echo "------START------"
#TODO: add extra mpirun flags, see mpirun --help, e.g. -cpus-per-rank
mpirun -n ${mpi_processes} --map-by node:PE=${OMP_NUM_THREADS} "${build_dir}main"
echo "-------END-------"
echo -e "run_id  : ${run_id}"
echo -e "Comments: ${comments}"
