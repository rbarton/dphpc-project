#!/usr/bin/env bash
set -a

#This script executes compiled program and prints nice output
#It also handles benching, executing multiple MPI/OMP configurations sequentially
#Is in a separate file to easily tee output

cd "${out_dir}"
echo -e "${DIM_FMT}------START------${RESET_FMT}"
if [[ "${runMode}" == 0 ]]; then                        #Single test mode
  mpiexec -n ${mpi_processes} "${build_dir}main"
else
  echo -e "${BOLD_FMT}${GREEN}Bench Mode${RESET_TEXT}, #configs: ${#mpi_processes_bench[@]}"
  if [ ${#mpi_processes_bench[@]} != ${#OMP_NUM_THREADS_bench[@]} ]; then
    echo -e "${RED}Bench Error: Inconsistent array lengths${RESET_COL}, mpi_processes_bench and OMP_NUM_THREADS_bench must match"
    return 1
  fi

  #Loop over all configurations in run.config
  for index in "${!mpi_processes_bench[@]}"; do
    echo -e "${RED}------- ${index} -------${RESET_COL}"
    echo "MPI Processes : ${mpi_processes_bench[$index]}"
    echo "OpenMP Threads: ${OMP_NUM_THREADS_bench[$index]}"

    #Search and replace values in run.config
    $sedcmd -i "s/OMP_NUM_THREADS=.*/OMP_NUM_THREADS=${OMP_NUM_THREADS_bench[$index]}/" "run.config"
    $sedcmd -i "s/mpi_processes=.*/mpi_processes=${mpi_processes_bench[$index]}/" "run.config"

    mpiexec -n ${mpi_processes_bench[$index]} "${build_dir}main"
  done
fi
echo -e "${DIM_FMT}------POST-------${RESET_FMT}"
"${root_dir}scripts/post_process.sh"
echo -e "${DIM_FMT}-------END-------${RESET_FMT}"

echo "run_id  : $run_id"
echo "Comments: $comments"
