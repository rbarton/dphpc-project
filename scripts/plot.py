import sys, os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

"""
Select data, use default bench.csv if no args
Or pass list of csv's to use
"""
if len(sys.argv) <= 1:
    print("Using Source: bench.csv (default)")
    data = pd.read_csv("../cmake-build-debug/bench_grid.csv")
else:
    print("Using Source: \n", sys.argv[1])
    data = pd.read_csv(sys.argv[1], skipinitialspace=True)
    for i in sys.argv[2:]:
        if os.path.exists(i):
            print(i)
            data = pd.concat([data, pd.read_csv(i, skipinitialspace=True)])
        else:
            print(i, " skipped")


sns.set_style("whitegrid")
def fixColors(ax, fill=False):
    """Changes box fill to transparent and edge to fill color"""
    for i,artist in enumerate(ax.artists):
        col = artist.get_facecolor()
        artist.set_edgecolor(col)
        if fill == False:
            artist.set_facecolor('None')
        for j in range(i*6,i*6+6):
            line = ax.lines[j]
            line.set_color(col)
            line.set_mfc(col)
            line.set_mec(col)
# print("Data Shape: ", data.shape)
# print(data.head(200))
# print(data.columns.tolist())

### Pre-Processing ###
#data[data['Method'].startswith('MPI')]['Method'] = 'MPI' + data[data['Method'] == 'MPI']['NumMPIProcs']
data.replace(to_replace='FringeSeq', value='Seq', inplace=True)
data.replace(to_replace='FringeOMP', value='OMP', inplace=True)
data.replace(to_replace='FringeOMPfs', value='OMPfs', inplace=True)
data.replace(to_replace='FringeMPI1p', value='OMP+MPI1p', inplace=True)
data.replace(to_replace='FringeMPI2p', value='OMP+MPI2p', inplace=True)
data.replace(to_replace='FringeMPI3p', value='OMP+MPI3p', inplace=True)
data.replace(to_replace='FringeMPI4p', value='OMP+MPI4p', inplace=True)
data.replace(to_replace='FringeMPI5p', value='OMP+MPI5p', inplace=True)
data.replace(to_replace='FringeMPI6p', value='OMP+MPI6p', inplace=True)
data.replace(to_replace='FringeMPI7p', value='OMP+MPI7p', inplace=True)
data.replace(to_replace='FringeMPI8p', value='OMP+MPI8p', inplace=True)
print(data.head())

seqRunTime = data[data['Method'] == 'Seq']['Runtime'].mean()
ompRunTimeOneCore = data[(data['NumCores'] == 1) & (data['Method'] == 'OMP')]['Runtime'].mean()
print('Mean Seq Runtime: ', seqRunTime)
print('Mean OMP 1t time: ', ompRunTimeOneCore)

seqLinearSpeedup = []
ompLinearSpeedup = []
for i in data['NumCores'].unique():
    seqLinearSpeedup.append(seqRunTime / i)
    ompLinearSpeedup.append(ompRunTimeOneCore / i)

maxCores = data['NumCores'].max()

data['Speedup'] = data['Runtime'].apply(lambda x: seqRunTime / x)

correctPathLength = data[data['Method'] == 'Seq']['PathLength'].min()
data['PathLengthError'] = ((data['PathLength']/correctPathLength -1)*100)


### Runtime Plot ###
ax1 = sns.boxplot(data=data, x=data['NumCores'], y=data['Runtime'], hue=data['Method'],
                  palette="muted", linewidth=1.0, dodge=False, saturation=0.8)
ax1.plot(seqLinearSpeedup, c="blue", ls=":", mec="gray", mew=0.1)
ax1.plot(ompLinearSpeedup, c="lightgreen", ls="--", mec="gray", mew=0.1)
ax1.set(yscale="log", ylabel='Runtime in seconds', xlabel='Total Cores', title='Sequential and Parallel Fringe Runtime\nAsia Road Graph, CostRelaxation=100')

import matplotlib.ticker as ticker
ax1.yaxis.set_major_locator(ticker.MultipleLocator(1))
ax1.yaxis.set_major_formatter(ticker.ScalarFormatter())

ax1.grid(True)

fixColors(ax1)

# ax1.legend(size=2)
plt.savefig("runtimePlot.pdf", bbox_inches='tight')
plt.clf()
print("runtime.")

### Speedup Plot ###
plt.clf()
ax2 = sns.boxplot(data=data, x='NumCores', y='Speedup', hue=data['Method'], dodge=False, linewidth=1.0)
ax2.set(ylabel='Speed Up', title='Speed Up in Relation to FringeSeq with one core\nAsia Road Graph, CostRelaxation=100')
ax2.plot(np.arange(1, maxCores), c=".2", ls="--")
ax2.set(ylim=[0, data['Speedup'].max() * 1.1])
ax2.grid(True)
fixColors(ax2)

plt.savefig("speedupPlot.pdf", bbox_inches='tight')
plt.clf()
print("speedup.")


### Correctness Plot ###
# Add Runtime Line
plt.clf()
data['RuntimeX'] = data['CostRelaxation']*5
ax3 = sns.lineplot(data=data, x='RuntimeX', y='Runtime', color='r', hue='NumCores', palette=sns.color_palette("pastel", 4))
ax3.set(yscale="log", ylabel='Runtime in Seconds', xlabel='Cost Relaxation')

# Add PathLengthError boxes
ax4 = ax3.twinx()

ax4 = sns.boxplot(data=data, x='CostRelaxation', y='PathLengthError', color="r", hue='NumCores', palette="deep")
ax4.set(ylabel='Error compared to true shortest Path in %', xlabel='Cost Relaxation', title='Relaxation Error on a 1,000x1,000 Grid Graph')

ax3.get_legend().remove()
fixColors(ax4)

plt.savefig("correctnessPlot.pdf", bbox_inches='tight')
plt.clf()
print("correctness.")

### FringeSizePlot ###
dataNoAstar=data[data['Method'] != 'Astar']
marker = ['o', 'x', '^', '+', '*', '8', 's', 'p', 'D', 'h', 'H', 'p', 'X']
markers = [marker[i % len(marker)] for i in range(len(dataNoAstar["NumCores"].unique()))]

plt.clf()
ax5 = sns.lmplot(data=dataNoAstar, x="AvgFringeSize", y="Runtime", hue="NumCores", truncate=True, markers=markers)
ax5.set(title='Runtime compared to the average fringe size')

plt.savefig("fringeSizePlot.pdf", bbox_inches='tight')
plt.clf()
print("fsize.")
