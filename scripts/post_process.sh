#!/usr/bin/env bash
# This script is called once the cpp executable has finished, use it to plot graphs etc
# It is executed in the out/run_id folder where the log file is

if [[ "${auto_generate_graph}" == 1 && ! -z "${graph_dot_file}" && -f "${graph_dot_file}" ]]; then
  graph_pdf_file="${graph_dot_file%.dot}.${graph_file_type}"
  dot -T"${graph_file_type}" "${graph_dot_file}" -o"${graph_pdf_file}"
  echo "Graph saved to: ${graph_pdf_file}"
fi

if [[ "${auto_plot_bench}" == 1 && ! -z "${bench_file}" && -f "${bench_file}" ]]; then
  python3 "${root_dir}scripts/plot.py" "${bench_file}"
fi