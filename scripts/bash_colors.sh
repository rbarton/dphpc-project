#!/usr/bin/env bash

#Adds variables to be used as color tags with 'echo -e'
if [ "${use_colors}" == "1" ]; then
  RESET_TEXT="\e[0m"
  RESET_COL="\e[39m"
  RED="\033[0;31m"
  GREEN="\e[32m"

  RESET_FMT="\e[0m"
  DIM_FMT="\e[2m"
  BOLD_FMT="\e[1m"
  BLINK_FMT="\e[1m\e[5m"
  echo -e "${GREEN}${BLINK_FMT}Colors Enabled${RESET_TEXT}"
fi
