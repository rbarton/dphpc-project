# Parallel Fringe Search

## Report

The [report-submitted.pdf](report-submitted.pdf) summarizes our findings in parallelizing fringe search.

**Input configurations and the data gathered for the report can be found in the `report-tests` directory for reproducibility.**

- `82,83` Runtime/Speedup plots
- `85` Road map runtime plot
- `90-100` Relaxation error plot
- `euler/117to133` plots generated from grid graph run, each folder represents one job
- `102` MPI load balancing plot

View the report of this project in the `report-submitted.pdf` file

## Get Started

In order to run create a local copy of run and environment config files. These store settings for compiling and running
```bash
cp run.config.default run.config
cp env.config.default env.config
```


Download extra roadmap graphs with:
 ```bash
wget https://polybox.ethz.ch/index.php/s/nMByqxEwWIwB9ru/download
unzip download
rm download
 ```

## Quick Commands
**Run `source load_scripts.sh` in every terminal session**

This will enable various quick commands to compile and run. Run `help_dphpc` to list all possible commands.

You can set various parameters for your environment in `env.config`
Passing parameters to the program can be done with `run.config`
Compiling & running the program normally is, of course, still possible.

*note: using `cmake_generator=Ninja` is recommended as it is much faster but can cause errors and is not available on the clusters. You will have to install it first `apt install ninja-build`*

## Setup - Libraries
Please install the following libraries:
- MPICH (or alternatively OpenMPI)
   - `sudo apt install libmpich-dev`
- Boost Graph Library (BGL)
   - `sudo apt install libboost-graph-dev libboost-graph-parallel-dev`
- CUDA Toolkit 10.1 *(optional)*
   - https://developer.nvidia.com/cuda-downloads
      - network (local) is recommended
   - Be sure to follow the instructions at the docs link, especially post-install instructions
   - check that `nvcc` command works afterwards
   - *This works even if you do not have an Nvidia GPU*
- Eigen install is *not* required, it is already in the include folder
- OpenMP (try this if your compiler does not have OpenMP already)
   - `sudo apt install libomp5`

Then ensure to copy the `run.config.default` to `run.config`, the same for `env.config`. These allow you to customize the process without adding the files to git. Ensure that the `.config` files are kept up-to-date with the `.default` files, i.e. if new variables are added.

Try running `cmake` or `compile` to ensure everything works

## Running on a Cluster

1. Run `euler` or `leo` to create an ssh session on a cluster login node.
2. Clone the repository.
	- You can add your git ssh keys as usual to the `~/.ssh` folder. 
3. Use the usual commands like `run` or `compile` but add `euler` or `leo` as the first argument.
4. To download the output files use the `download_data` command. Do this on your *local machine* (not in the ssh session)
	- ensure that the `root_dir_on_cluster` in `env.config` is set appropriately

If there are problem with modules or CMake you may have to adjust the `scripts/load_modules.sh` files.

If you want to customize how the program is executed edit the`run.config`, else adjust the `scripts/submit_job.sh` script.

### Best Thread Setup

In case of using only CPUs:

- See https://scicomp.ethz.ch/wiki/Parallel_computing#Hybrid_jobs
- Ensure #cores per node is divisible by #omp threads

With GPUs:

- One MPI process per GPU, works best in most cases. Use a stream/s to post computations to the GPU, any OpenMP thread can add work to the 'queue', the GPU will 
- Technically faster if one process has several GPUs, but is more difficult. One MPI process per node

*Other configurations may be faster...*

## Boost Graph Library

We use the BGL for the graph data structures. See:

- [Overview of library](https://www.boost.org/doc/libs/1_46_1/libs/graph/doc/quick_tour.html)
- [How A* is done](https://www.boost.org/doc/libs/1_43_0/libs/graph/example/astar-cities.cpp)
  - This also includes an example of how to print the graph to **GraphViz** (`.dot`file)
  - Use `dot -Tpdf -O graph.dot` to generate a pdf afterwards quickly
  - Or open the `.dot` file in **Gephi** *(recommended)* 

## CUDA Overview

Set `ENABLE_CUDA` in the CMake Cache, to compile cuda code.

All cuda kernels/code is in a separate *optional* library `cuda_kernels`. It can be called via the `cuda_kernels.h` header functions. Only add non-cuda functions to the header, as it is included in the non-cuda compiled files. All cuda code
must explicitly be in a `.cu` or `.cuh` file in this library.

This method allows all cuda code to be optionally compiled, if the cuda toolkit is available. A **key macro `CUDA_ENABLED`**
will be set, use this to optionally call cuda functions through the `cuda_kernels.h` interface. e.g.

```cpp
//main.cpp
#ifdef CUDA_ENABLED
	#inculde "cuda_kernels.h"
	cuda_kernels::CudaInterfaceFunction(...);
#else
	//alternative code
#endif
```