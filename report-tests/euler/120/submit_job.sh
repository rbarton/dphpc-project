#!/bin/bash -l
#submit with bsub < submit_job.sh
#execute several commands as part of a script
#Note! environment variables will be pasted before submitting to bsub

#N=M*T = total threads
#Paramters for the bsub command
#BSUB -n 4
#BSUB -R "span[ptile=24] rusage[mem=1024]"
#BSUB -N                    #receive email on finish
#BSUB -oo log.%J
#BSUB -W 01:00

hostname
lscpu

echo "MPI Processes : 1"
echo "OpenMP Threads: 4"
echo "Total Threads : 4"
echo "GPUs per Node : 0"
echo "------START------"
#TODO: add extra mpirun flags, see mpirun --help, e.g. -cpus-per-rank
mpirun -n 1 --map-by node:PE=4 "/cluster/home/rbarton/dphpc-project/run/main"
echo "-------END-------"
echo -e "run_id  : 120"
echo -e "Comments: euler report data"
