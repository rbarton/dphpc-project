#!/bin/bash -l
#submit with bsub < submit_job.sh
#execute several commands as part of a script
#Note! environment variables will be pasted before submitting to bsub

#N=M*T = total threads
#Paramters for the bsub command
#BSUB -n 6
#BSUB -R "span[ptile=24] rusage[mem=1365]"
#BSUB -N                    #receive email on finish
#BSUB -oo log.%J
#BSUB -W 01:00

hostname
lscpu

echo "MPI Processes : 2"
echo "OpenMP Threads: 3"
echo "Total Threads : 6"
echo "GPUs per Node : 0"
echo "------START------"
#TODO: add extra mpirun flags, see mpirun --help, e.g. -cpus-per-rank
mpirun -n 2 --map-by node:PE=3 "/cluster/home/rbarton/dphpc-project/run/main"
echo "-------END-------"
echo -e "run_id  : 127"
echo -e "Comments: euler report data"
