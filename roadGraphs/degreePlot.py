
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)
import matplotlib.pyplot as plt
import numpy as np

degrees=[1,2,3,4,5,6,7,8,9,10,11,12,13]
degreeCount = [2055578,41533718,6329373,970917,20427,1789,156,33,8,2,4,11,2]
men_means = [20, 34, 30, 35, 27]
women_means = [25, 32, 34, 20, 25]


width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects = ax.bar(degrees,np.log(degreeCount) , width)


# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('log(count)')
ax.set_title('Degree histogram')
ax.set_xlabel('degree')
ax.set_xticks(degrees)


def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for ind,rect in enumerate(rects):
        height = rect.get_height()
        ax.annotate('{}'.format(degreeCount[ind]),
                    xy=(rect.get_x() + rect.get_width() / 2, height-0.2),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

# string = "average Degree: "+ str((np.sum((np.multiply(degreeCount,degrees)))/np.sum(degreeCount)))
string = "average degree: " + '{0:.3f}'.format((np.sum((np.multiply(degreeCount,degrees)))/np.sum(degreeCount)))
offsetbox = TextArea(string, minimumdescent=False)

ab = AnnotationBbox(offsetbox, [12,12],
                    xybox=(-20, 40),
                    xycoords='data',
                    boxcoords="offset points")
ax.add_artist(ab)


autolabel(rects)

fig.tight_layout()

# plt.show()
plt.savefig("degreeHistoLog.png")






# import numpy as np
# import matplotlib.pyplot as plt
#
#
# N = 5
#
# ind = np.arange(N)    # the x locations for the groups
# width = 0.35       # the width of the bars: can also be len(x) sequence
#
# fig, p1 = plt.subplots()
#
# degreeCount = [2055578,41533718,6329373,970917,20427,1789,156,33,8,2,4,11,2]
# degrees=[1,2,3,4,5,6,7,8,9,10,11,12,13]
# # p1 = plt.bar(degrees, degreeCount, width, )
# p1 = plt.bar(degrees, np.log(degreeCount), width, )
#
# # degree: 1  count: 2055578
# # degree: 2  count: 41533718
# # degree: 3  count: 6329373
# # degree: 4  count: 970917
# # degree: 5  count: 20427
# # degree: 6  count: 1789
# # degree: 7  count: 156
# # degree: 8  count: 33
# # degree: 9  count: 8
# # degree: 10  count: 2
# # degree: 11  count: 4
# # degree: 12  count: 11
# # degree: 13  count: 2
#
#
#
# plt.ylabel('log(count)')
# plt.xlabel('degree')
# plt.xticks(degrees)
# plt.title('Degree histogram')
# # plt.legend((np.average(np.prod([[degreeCount],[degrees]],axis=0)))("avg"))
#
# # plt.savefig("degreeHisto.png")
# plt.savefig("degreeHistoLog.png")