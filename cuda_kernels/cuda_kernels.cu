#include "cuda_kernels.h"
#include <iostream>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

namespace cuda_kernels{
    __global__ void PrintKernel(){
        printf("kernel called\n");
    }

    void TestCuda(){
        std::cout << "calling kernel...";
        PrintKernel<<<1,1>>>();
        cudaDeviceSynchronize();
    }
}