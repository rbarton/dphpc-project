#pragma once

// Define all public/interface functions to be called from a non-cuda context
// Do not use any cuda coda here, create a small 'interface' function if needed

namespace cuda_kernels {
    void TestCuda ();
}