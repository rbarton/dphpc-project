#!/usr/bin/env bash
set -a   #auto export all variables to the environment, used only for envsubst command
#This script loads various variables and functions into the shell when sourced
#It includes various handy run, compile (with c/make), plot, clean functions
#Each function generally also has an according _help function
#Ensure to maintain working directory independence by adding root_dir infront of files being used etc.

#TODO: add bench function to call list of mpiprocs/ompthreads
function help_dphpc() {
  echo "Usage: Run script with 'source' instead of ./ within same directory(!)"
  echo "       Script will enable use of various functions in current shell"
  echo "       Functions can be called from any directory"
  echo ""
  echo -e "Examples \U0001F928:"
  echo -e "  source scripts.sh    #Must run this first, in each shell instance \U0001F612"
  echo -e "  ${BOLD_FMT}run${RESET_FMT}"
  echo -e "  ${BOLD_FMT}run_again${RESET_FMT}"
  echo    "  compile"
  echo    "  run_only"
  echo -e "  ${BOLD_FMT}download_data${RESET_FMT}        #Get output from a execution on a cluster"
  echo    "  plot"
  echo    "  clean_*"
  echo    "Helper Functions:"
  echo    "  euler                #ssh to Euler"
  echo    "  leo                  #ssh to Leonhard"
  echo    "  scp2cluster          #Manual upload to cluster"
  echo    "  scp2here             #Manual download from cluster"
  echo -e "  ${DIM_FMT}clusters_add_ssh_key #login without password to clusters${RESET_FMT}"
  echo    "  *_help               #Exists for most functions"
  echo -e "  ${BOLD_FMT}help_dphpc${RESET_FMT}           #Shows this again"
}

if [[ "$0" == "$BASH_SOURCE" ]] ; then  #Checks if script was run with ./ (vs source)
  help_dphpc
  exit
fi

#Use absolute paths, functions can be called from anywhere
root_dir="$(dirname "$PWD/$BASH_SOURCE")/"      #get path of script
source "${root_dir}env.config"   #load customisable variables
source "${root_dir}scripts/bash_colors.sh"    #import colors tags
echo -e "${DIM_FMT}Using Project Root: $root_dir${RESET_FMT}"

build_dir="${root_dir}${build_dir_name}"
out_root="${root_dir}${out_dir_name}"

#import functions for indexing of builds
source "${root_dir}scripts/run_id.sh"

#show help if this is the first run
if [[ ! -d "${out_root}" && ! -d "${build_dir}" ]]; then
  help_dphpc
fi

#-------Begin of Functions--------------------------
function run_help() {
  echo -e "Usage : ${BOLD_FMT}run [euler | leo] [Comments]${RESET_FMT}"
  echo    "        If 1st arg is euler or leo, a job will be scheduled"
  echo    "Output: Will compile and run the main project"
  echo    "        Use run_only to skip re-compilation"
  echo    "        Output is saved in '${out_dir#"${root_dir}"}'"
  echo    "        Enter euler shell with command 'euler' (ensure to set nethz)"
  echo -e "        ${BOLD_FMT}Use run_again to keep same run_id${RESET_FMT}"
}

function run() {
	increment_run_id
  run_again "$@"
}

function run_again() {
  compile $1 || return 1
  run_only "$@"
}

function compile_help() {
  echo -e "Usage: ${BOLD_FMT}compile [euler | leo]${RESET_FMT}"
  echo    "       Runs cmake then buids the executable with the set 'cmake_generator'"
  echo    "       Passing a cluster name will also ensure the modules are loaded"
}

function compile() {
  #import latest variables
	source "${root_dir}run.config"

  if [[ $1 == *"euler"* || $1 == *"leo"* ]]; then          #note: $1 contains euler or leo, excludes whitespace
	  source "${root_dir}scripts/load_modules.sh" $1 || { echo "Failed to load modules, are you in the euler/leo ssh?" && return 1; }
  fi

  #Usual compile process, adapt to generator specified in environment
  mkdir -p "${build_dir}"
  (cd "${build_dir}"
  cmake -G"${cmake_generator}" -DENABLE_CUDA=${enable_cuda} .. || exit 1
  if [ "${cmake_generator}" == "Ninja" ]; then
    ninja
  else
	  make -j${build_processes}     #use brackets to execute in a subshell so we dont care about the cd
  fi) || return 1
}

function run_only_help() {
  echo -e "Usage : ${BOLD_FMT}run_only [euler | leo] [Comments]${RESET_FMT}"
  echo    "Output: Runs the compiled code, with the current run_id"
  echo    "        You may want to use increment_run_id before"
  echo    "        Displays comments at end"
}

function run_only() {
	#get the current id and create out_dir
	get_run_id
	update_out_dir
	mkdir -p "${out_dir}"

	source "${root_dir}run.config" #import current run.config
	cp "${root_dir}run.config" "${out_dir}"
  comments="${@##euler}"
  comments="${@##leo}"
  echo "#Comments: ${comments}" >> "${out_dir}run.config"

	if [[ "$1" == *"euler"* || "$1" == *"leo"* ]]; then
    cd "${out_dir}" || return #Note!
	  #Schedule job with batch system, run in out_dir

    #copy run script to here and substitute variables, add gpu request if on leo
    if [[ "${enable_cuda}" != "ON" ]]; then
      gpus_per_node="0"
    fi

    mem_per_cpu=$(($mem_required / $OMP_NUM_THREADS))
    if [[ "$1" == *"leo"* ]]; then
      bsub_R_flags="rusage[mem=${mem_per_cpu},ngpus_excl_p=${gpus_per_node}]"     #span[ptile=${processes_per_node}]
    else
      bsub_R_flags="rusage[mem=${mem_per_cpu}]"
    fi
    total_threads=$((${mpi_processes} * ${OMP_NUM_THREADS}))      #required for run script
    envsubst < "${root_dir}scripts/submit_job.sh" > submit_job.sh

    bsub < submit_job.sh

    echo -e "${GREEN}${BOLD_FMT}run_id  : $run_id${RESET_FMT}${RESET_COL}"
	  bjobs
    cd -
  else
    #Stop if we are running on a login node, need to schedule a job
    if [[ "$(echo "$PWD" | cut -d "/" -f2)" == *"cluster"* ]]; then
        echo -e "${RED}${BOLD_FMT}You should not run your code on a login node, aborting...${RESET_FMT}${RESET_COL}"
        echo -e "Pass 'euler' or 'leo' as the first argument."
        run_help
        return 1
    fi

    echo -e "${GREEN}${BOLD_FMT}run_id  : $run_id${RESET_FMT}${RESET_COL}"
    #run with mpiexec, and copy output using tee to the log file
    source ${root_dir}scripts/run_local.sh 2>&1 | tee "${out_dir}/log"
	fi
}

#------Euler Functions-----------
function benchcluster_help() {
  echo -e "Usage : ${BOLD_FMT}benchcluster <euler | leo> [comments]${RESET_FMT}"
  echo    "Effect: Calls run several times with varying MPI/OMP threads"
  echo    "        So saves results into several different run_ids (jobs work in parallel)"
  echo    "        Do not use on your local machine."
  echo -e "        To plot you can use: ${BOLD_FMT}python3 scripts/plot.py out/{1,2,3}/bench.csv${RESET_FMT}"
}

function benchcluster() {
  #same checks as in run_local.sh
  source "${root_dir}run.config"

  echo -e "${BOLD_FMT}${GREEN}Bench Mode${RESET_TEXT}, #configs: ${#mpi_processes_bench[@]}"
  if [ ${#mpi_processes_bench[@]} != ${#OMP_NUM_THREADS_bench[@]} ]; then
    echo -e "${RED}Bench Error: Inconsistent array lengths${RESET_COL}, mpi_processes_bench and OMP_NUM_THREADS_bench must match"
    return 1
  fi

  compile $1 || return 1

  source "${root_dir}run.config"
  for index in "${!mpi_processes_bench[@]}"; do     #Iterate over list and run_only each time, increment id manually
    echo -e "${RED}------- ${index} -------${RESET_COL}"
    echo "MPI Processes : ${mpi_processes_bench[$index]}"
    echo "OpenMP Threads: ${OMP_NUM_THREADS_bench[$index]}"

    #Search and replace values in run.config
    $sedcmd -i "s/OMP_NUM_THREADS=.*/OMP_NUM_THREADS=${OMP_NUM_THREADS_bench[$index]}/" "run.config"
    $sedcmd -i "s/mpi_processes=.*/mpi_processes=${mpi_processes_bench[$index]}/" "run.config"

    increment_run_id
    run_only "$@"
  done
}

function download_data_help() {
  echo -e "Usage : ${BOLD_FMT}download_data <euler | leo> <run_id> [run_id...]${RESET_FMT}"
  echo    "        run_id - which output to download from cluster"
  echo    "Effect: Downloads the output data from euler/leonhard and saves it into '${out_dir_name}<cluster>/<run_id>/'"
  echo    "        Assumes that the project on the cluster is in ~/dphpc-project"
  echo    "Tip   : download_data euler \$(echo {39..66..1}) to download a range"

}

function download_data() {
  #Find run_id we want to download
  if [[ -z "$1" && -z "$2" ]]; then
    download_data_help
    return
  fi
  run_id=$2

  #set source and target destination relative to project root
  download_dest="${out_root}${1}/${run_id}"
  mkdir -p "${download_dest}"
  update_out_dir relative   #where to download on the cluster

  echo -e "Downloading to: ${BOLD_FMT}${download_dest#"${root_dir}"}${RESET_FMT}"
  echo -e "          from: ${DIM_FMT}~/${root_dir_on_cluster}${out_dir}${RESET_FMT}"
	scp2here $1 "${root_dir_on_cluster}${out_dir}*" "${download_dest}"

	if [[ -n "$3" ]]; then #recursive
    download_data "$1" "${@:3}"
  fi
}

#--------Helper Functions-----------
function plot() {
  get_run_id
  source "${root_dir}run.config"
  update_out_dir
  python3 "${root_dir}scripts/plot.py" "${out_dir}${bench_file}"
}

function generategraph() { #TODO: improve this to be outdir local
  dot -Tpdf graph.dot -ograph.pdf
}

function clean_project() {
    clean_build
    clean_out
}

function clean_build() {
	rm -r "${build_dir}"
}

function clean_out() {
  rm -r "${out_root}"
}

#Updates the out_dir to take into account the current run_id
#Arg1: optional 'relative' to set a relative path to project root
function update_out_dir() {
  if [[ $1 == *"relative"* ]]; then
    #need to use relavtive paths, so remove root prefix
    out_dir="${out_dir_name}${run_id}/"   #update out dir
  else
    out_dir="${out_root}${run_id}/"   #update out dir
  fi
}

#----Extra Euler Helper Functions-----------
function euler() {
  if [ -z ${nethz} ]; then
    echo -e "Please ${BOLD_FMT}set your nethz${RESET_FMT} in env.config or in your .bashrc"
  else
    ssh ${nethz}@euler.ethz.ch
  fi
}

function leo() {
  if [ -z ${nethz} ]; then
    echo -e "Please ${BOLD_FMT}set your nethz${RESET_FMT} in env.config or in your .bashrc"
  else
    ssh ${nethz}@login.leonhard.ethz.ch
  fi
}

function scp2cluster(){
  if [ -z ${nethz} ]; then
    echo -e "Please ${BOLD_FMT}set your nethz${RESET_FMT} in env.config or in your .bashrc"
    return 1
  elif [[ -z "$1" || -z "$2" || -z "$3" ]]; then
    echo -e "Usage : ${BOLD_FMT}scp2cluster <euler | leo | slab1> <source> <destination>${RESET_FMT}"
    echo    "Output: scp <source> ${nethz}@<cluster>:/cluster/home/${nethz}/<destination>"
    return 1
  fi

  get_cluster_login $1
  scp -r "${2}" "${nethz}@${cluster_login}.ethz.ch:/cluster/home/${nethz}/${3}"
}

function scp2here(){
  if [ -z ${nethz} ]; then
    echo -e "Please ${BOLD_FMT}set your nethz${RESET_FMT} in env.config or in your .bashrc"
    return 1
  elif [[ -z "$1" || -z "$2" ]]; then
    echo -e "Usage : ${BOLD_FMT}scp2here <euler | leo | slab1> <source> [destination]${RESET_FMT}"
    echo    "Output: scp ${nethz}@<cluster>:/cluster/home/${nethz}/<source> <destination | .>"
    echo    "        Empty destination downloads to current directory"
    return 1
  fi

  # Use current directory if its empty
  destination="${3}"
  if [[ -z "$3" ]]; then
    destination=.
  fi

  get_cluster_login $1
  scp -r "${nethz}@${cluster_login}.ethz.ch:/cluster/home/${nethz}/${2}" "${destination}"
}

function get_cluster_login() {
    if [[ $1 == *"leo"* ]]; then
    cluster_login="login.leonhard"
  else
    cluster_login=$1
  fi
}

function clusters_add_ssh_key() {
  echo "This command will add your 'id_rsa.pub' ssh key to euler, leonhard and slabs"
  echo "This saves you having to enter your password each time"
  read -r -p "Confirm? [y/N] " response
  if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    echo cat $HOME/.ssh/id_rsa.pub \| ssh ${nethz}@euler.ethz.ch "cat - >> .ssh/authorized_keys"
    cat $HOME/.ssh/id_rsa.pub | ssh ${nethz}@euler.ethz.ch "cat - >> .ssh/authorized_keys"
    echo cat $HOME/.ssh/id_rsa.pub \| ssh ${nethz}@login.leonhard.ethz.ch "cat - >> .ssh/authorized_keys"
    cat $HOME/.ssh/id_rsa.pub | ssh ${nethz}@login.leonhard.ethz.ch "cat - >> .ssh/authorized_keys"
    echo cat $HOME/.ssh/id_rsa.pub \| ssh ${nethz}@slab1.ethz.ch "cat - >> .ssh/authorized_keys"
    cat $HOME/.ssh/id_rsa.pub | ssh ${nethz}@slab1.ethz.ch "cat - >> .ssh/authorized_keys"
  fi
}